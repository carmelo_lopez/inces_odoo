# coding: utf-8
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Evaluación Docente INCES',
    'version': '10.0.1.0.0',
    'author': 'FaCyT',
    'maintainer': 'FaCyT',
    'website': 'https://gitlab.com/carmelo_lopez/inces_odoo',
    'license': 'AGPL-3',
    'category': 'Extra Tools',
    'summary': 'Evaluación de desempeño docente a facilitadores INCES',
    'depends': ['control_estudios', 'asistencias'],
    'data': [
        # Seguridad
        'security/groups.xml',
        # Reglas de acceso
        'security/ir.model.access.csv',
        # Vistas comunes
        'views/evaluacion.xml',
        'views/facilitador.xml',
        'views/clase.xml',
        'views/menues.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'demo': [
    ],
}
