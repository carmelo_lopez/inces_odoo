from odoo import fields, models


class Facilitador(models.Model):
    _name = 'facilitador'
    _inherit = 'facilitador'

    evaluacion_ids = fields.One2many(
        'facilitador.evaluacion', 'facilitador_id', 'Evaluación Docente')

    def get_ultima_evaluacion(self):
        """
        Devuelve la ultima evaluacion en estado 'finalizada'.
        """
        ultima_evaluacion = False
        evaluaciones = self.evaluacion_ids.filtered(
            lambda r: r.state == 'finalizada').sorted(
                key=lambda r: (r.fecha_evaluacion, r.fecha_fin))
        if evaluaciones:
            ultima_evaluacion = evaluaciones[-1]
        return ultima_evaluacion

    def get_clases_impartidas(self, unidad_curricular):
        ultima_evaluacion = self.get_ultima_evaluacion()
        clases_impartidas = 0
        if ultima_evaluacion:
            clases_impartidas = self.env['asistencia'].search_count([
                ('facilitador_id', '=', self.id),
                ('unidad_id', '=', unidad_curricular.id),
                ('fecha_clase', '>=', ultima_evaluacion.fecha_inicio),
                ('fecha_clase', '<=', ultima_evaluacion.fecha_fin),
                ('clase_impartida', '=', True),
                ('state', '=', 'aprobada'),
            ])
        return clases_impartidas
