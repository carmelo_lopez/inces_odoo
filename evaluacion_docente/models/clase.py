from odoo import api, fields, models


class Clase(models.Model):
    _name = 'clase'
    _inherit = 'clase'

    MAX_FACILITADORES_RECOMENDADOS = 5

    facilitador_recomendado_ids = fields.One2many(
        'facilitador.recomendado',
        'clase_id',
        string='Facilitadores recomendados')

    def get_ponderaciones_evaluacion(self):
        record = self.env['evaluacion.ponderaciones.recomendacion'].search(
            [('unidad_id', '=', self.unidad_id.id)])
        if record:
            ponderaciones = record.get_ponderaciones()
        else:
            ponderaciones = record.get_ponderaciones(get_defaults=True)

        return ponderaciones

    def get_media_ponderada_evaluacion(
            self, facilitador, ponderaciones, sum_ponderaciones):
        """Calculo: suma de los factores evaluados multiplicados cada uno
        por la ponderacion (peso) definido para ese factor y el resultado es
        dividido entre la suma de las ponderaciones.
        """
        media_ponderada = 0
        ultima_evaluacion = facilitador.get_ultima_evaluacion()
        if ultima_evaluacion:
            puntuacion = 0
            for factor, ponderacion in ponderaciones.items():
                puntuacion += ponderacion * getattr(ultima_evaluacion, factor)
            media_ponderada = puntuacion / sum_ponderaciones

        return media_ponderada

    def get_puntuacion_facilitadores(self, facilitadores):
        """
        Devuelve un diccionario 'recomendados_puntuacion' donde:
        -) La llave es el registro del facilitador
        -) El valor es el puntuacion de ese facilitador. Se obtiene con la
        media ponderada de los factores de su evaluación
        Los facilitadores sin evaluaciones tienen puntuacion = 0
        """
        recomendados_puntuacion = {}
        ponderaciones = self.get_ponderaciones_evaluacion()
        sum_ponderaciones = sum(ponderaciones.values())
        for facilitador in facilitadores:
            recomendados_puntuacion[facilitador] = \
                self.get_media_ponderada_evaluacion(
                    facilitador, ponderaciones, sum_ponderaciones)

        recomendados_puntuacion = sorted(
            recomendados_puntuacion.items(), key=lambda kv: kv[1])
        recomendados_puntuacion.reverse()

        return recomendados_puntuacion[:self.MAX_FACILITADORES_RECOMENDADOS]

    def get_facilitadores_recomendados(self):
        unidad_curricular = self.unidad_id
        recomendados_obj = self.env['facilitador.recomendado']
        clases = self.env['clase'].search(
                [('state', 'in', ['cursando', 'terminado']),
                 ('unidad_id', '=', unidad_curricular.id)])
        recomendado_ids = []
        if clases:
            facilitadores = clases.mapped('facilitador_id') or []
            recomendados_puntuacion = self.get_puntuacion_facilitadores(
                facilitadores)
            for recomendado in recomendados_puntuacion:
                facilitador = recomendado[0]
                recomendado_ids += [
                    recomendados_obj.create({
                        'clase_id': self.id,
                        'facilitador_id': facilitador.id,
                        'puntuacion': recomendado[1],
                        'clases_impartidas': facilitador.get_clases_impartidas(
                            unidad_curricular)
                    }).id]
        return recomendado_ids

    @api.onchange('unidad_id')
    def _onchange_unidad_curricular(self):
        """
        Actualiza tabla de facilitadores recomendados para ser asignados
        a la clase
        """
        if self.unidad_id:
            recomendado_ids = self.get_facilitadores_recomendados()
            self.facilitador_recomendado_ids = [(6, 0, recomendado_ids)]
        else:
            self.facilitador_recomendado_ids = [(6, 0, [])]

    @api.multi
    def write(self, vals):
        super(Clase, self).write(vals)

        # Eliminar registros de facilitadores recomendados que no tengan
        # una clase asociada
        recomendados = self.env['facilitador.recomendado'].search(
            [('clase_id', '=', False)])
        recomendados.unlink()

        return True


class FacilitadorRecomendado(models.Model):
    _name = 'facilitador.recomendado'
    _description = 'Facilitadores recomendados para impartir una clase'
    _rec_name = 'facilitador_id'

    clase_id = fields.Many2one('clase', 'Clase')
    facilitador_id = fields.Many2one('facilitador', 'Facilitador')
    puntuacion = fields.Float(
        "Puntuación", digits=(1, 1), required=True, default=0.0)
    clases_impartidas = fields.Integer(
        "Clases Impartidas", required=True, default=0, help="Muestra la \
        cantidad de clases (de esta unidad curricular) impartidas por el \
        facilitador durante el período de la última evaluación docente")
