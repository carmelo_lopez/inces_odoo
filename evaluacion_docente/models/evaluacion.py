from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class FacilitadorActividad(models.Model):
    _name = 'facilitador.evaluacion.actividad'
    _description = 'Actividad realizada por el facilitador'

    evaluacion_id = fields.Many2one(
        'facilitador.evaluacion', ondelete='cascade', required=True)
    name = fields.Char('Actividad', required=True)
    tiempo_ejecucion = fields.Selection(
        string='Tiempo de Ejecución', required=True,
        copy=False, selection=[
            (3, 'Muy bueno'),
            (2, 'Bueno'),
            (1, 'Deficiente'),
        ]
    )


class FacilitadorEvaluacion(models.Model):
    _name = 'facilitador.evaluacion'
    _description = 'Actuación del Facilitador'
    _rec_name = 'facilitador_id'
    _order = 'fecha_evaluacion desc, fecha_inicio desc, fecha_fin asc'
    # _inherit = ['mail.thread']

    facilitador_id = fields.Many2one(
        'facilitador', 'Facilitador', required=True)
    genero = fields.Selection(
        related='facilitador_id.genero', store=False)
    fecha_ingreso = fields.Date(
        related='facilitador_id.fecha_ingreso', store=False)
    tipo_id = fields.Many2one(
        'facilitador.tipo', 'Cargo Actual', required=True)

    fecha_inicio = fields.Date('Inicio', required=True, help="Fecha de inicio \
        del período de evaluación")
    fecha_fin = fields.Date('Fin', required=True, help="Fecha fin del \
        período de evaluación")
    fecha_evaluacion = fields.Date('Fecha de la evaluación', readonly=True)
    state = fields.Selection(
        string='Estado', default='borrador', required=True, copy=False,
        track_visibility='onchange',
        selection=[
            ('borrador', 'Borrador'),
            ('en_revision', 'En revisión'),
            ('finalizada', 'Finalizada'),
        ]
    )

    # Actividades
    actividad_ids = fields.One2many(
        'facilitador.evaluacion.actividad', 'evaluacion_id',
        string='Actividades', copy=False)

    # Actuación del facilitador
    adaptabilidad = fields.Selection(
        string='Adaptabilidad', required=True, default=3,
        copy=False, selection=[
            (3, '3 - Se adapta de inmediato al cambio. Domina nuevas \
                actividades'),
            (2, '2 - Se adapta con un minimo de dificultad. Domina nuevas \
                actividades'),
            (1, '1 - Tiene resistencia al cambio y a las nuevas situaciones'),
        ]
    )
    seguridad = fields.Selection(
        string='Seguridad en sí mismo', required=True, default=3,
        copy=False, selection=[
            (3, '3 - Se observa con confianza en sí mismo. Controla \
                adecuadamente las situaciones'),
            (2, '2 - Se observa con confianza en sí mismo. Aún con cierta \
                reserva'),
            (1, '1 - Se observa inseguro, desconfiado. Pierde el control en \
                sus planteamientos'),
        ]
    )
    sociabilidad = fields.Selection(
        string='Sociabilidad', required=True, default=3,
        copy=False, selection=[
            (3, '3 - Excelente interacción y afectividad con los demás'),
            (2, '2 - Interactúa con otros con cierto grado de dificultad'),
            (1, '1 - Interactúa con dificultad y cierta timidez para con los \
                demás'),
        ]
    )
    madurez = fields.Selection(
        string='Madurez', required=True, default=3,
        copy=False, selection=[
            (3, '3 - Resuelve objetiva y oportunamente los problemas'),
            (2, '2 - Resuelve los problemas con dificultad. Le cuesta ser \
                objetivo. Priva el criterio lógico'),
            (1, '1 - Le cuesta resolver los problemas de manera oportuna. No \
                toma a tiempo las decisiones. Le cuesta ser objetivo'),
        ]
    )
    metas = fields.Selection(
        string='Metas', required=True, default=3,
        copy=False, selection=[
            (3, '3 - Manifiesta mucho interés en el logro de las metas a \
                corto, mediano y largo plazo'),
            (2, '2 - Medianamente manifiesta mucho interés en el logro de las \
                metas a corto, mediano y largo plazo'),
            (1, '1 - No manifiesta ningún interés hacia el logro de las metas \
                a corto, mediano y largo plazo'),
        ]
    )
    liderazgo = fields.Selection(
        string='Liderazgo', required=True, default=3,
        copy=False, selection=[
            (3, '3 - Tiene capacidad de persuasión, domino del grupo. Dirige \
                exitosamente el trabajo en equipo'),
            (2, '2 - Mediana capacidad para dirigir con éxito el trabajo en \
                equipo. Poca capacidad de persuasión y dominio'),
            (1, '1 - Poca capacidad de persuasión, mando y manejo de grupo'),
        ]
    )
    motivacion = fields.Selection(
        string='Motivación', required=True, default=3,
        copy=False, selection=[
            (3, '3 - Alto grado de motivación. Se interesa más por la \
                satisfacción del logro que por la recompensa externa'),
            (2, '2 - Impulsado por el logro de recompensas externas para \
                sentirse motivado'),
            (1, '1 - Demuestra poco impulso para el logro de metas. Indiferente \
                ante las recompensas'),
        ]
    )

    # Asistencia
    dias_laborados = fields.Integer(
        'Días laborados', compute='_compute_asistencias_ausencias', store=True)
    dias_no_laborados = fields.Integer(
        'Días no laborados', compute='_compute_asistencias_ausencias',
        store=True)
    clases_impartidas_qty = fields.Integer(
        'Clases Impartidas', compute='_compute_asistencias_ausencias',
        store=True)
    clases_no_impartidas_qty = fields.Integer(
        'Clases No Impartidas', compute='_compute_asistencias_ausencias',
        store=True)
    clases_reposo_qty = fields.Integer(
        'Clases No Impartidas por Reposo',
        compute='_compute_asistencias_ausencias', store=True)
    cumplimiento_horario = fields.Boolean('Cumplimiento del horario laboral')

    # Areas de voluntariado
    voluntario_salud = fields.Boolean('Salud', help='Acciones en las \
        instituciones de salud, hospitales, jornadas de vacunación, etc.')
    voluntario_social = fields.Boolean('Social', help='Acciones en el campo \
        social, consejos comunales, labores comunitarias, visitas a los \
        ancianatos, penitenciarias, etc.')
    voluntario_academica = fields.Boolean('Académica', help='Acciones \
        orientadas a foros, cursos, charlas de acuerdo a las necesidades \
        de la población')

    observaciones = fields.Text('Observaciones del supervisor', copy=False)

    resultado = fields.Selection(
        string='Resultado', default='0', copy=False, selection=[
            (0, 'Sin Evaluar'),
            (1, 'Deficiente'),
            (2, 'Bueno'),
            (3, 'Muy bueno'),
        ], help="Para ser llenado por la Gerencia General de Recursos Humanos"
    )
    resultado_editable = fields.Boolean(compute='_resultado_editable')

    _sql_constraints = [
        (
            'unique_fechas',
            'unique(facilitador_id, fecha_inicio, fecha_fin)',
            _('Ya existe una evaluación para el facilitador en el mismo \
            período.')
        ),
    ]

    def _resultado_editable(self):
        es_gerente = self.env.user.has_group(
            'evaluacion_docente.group_gerente_recursos_humanos')
        es_director = self.env.user.has_group(
            'control_estudios.group_director')
        en_revision = self.state == 'en_revision'
        self.resultado_editable = (es_gerente or es_director) and en_revision

    @api.depends('fecha_inicio', 'fecha_fin', 'facilitador_id')
    def _compute_asistencias_ausencias(self):
        asistencia_obj = self.env['asistencia']
        for record in self:
            if record.fecha_inicio and record.fecha_fin and \
                    record.facilitador_id:
                hojas_asistencia = asistencia_obj.search([
                    ('facilitador_id', '=', record.facilitador_id.id),
                    ('fecha_clase', '>=', record.fecha_inicio),
                    ('state', '=', 'aprobada'),
                ])

                hojas_asistente = hojas_asistencia.filtered(
                    'facilitador_asistente')

                record.clases_impartidas_qty = len(
                    hojas_asistente.filtered('clase_impartida'))
                dias_laborados = set(
                    hojas_asistente.filtered(
                        lambda r: r.clase_impartida).mapped(
                            'fecha_clase'))
                record.dias_laborados = len(dias_laborados)

                hojas_inasistente = hojas_asistencia - hojas_asistente

                record.clases_no_impartidas_qty = len(
                    hojas_asistencia.filtered(
                        lambda r: not r.clase_impartida))
                record.clases_reposo_qty = len(hojas_inasistente.filtered(
                    lambda r: r.motivo_inasistencia == 'reposo'))

                record.dias_no_laborados = len(set(
                    hojas_inasistente.mapped('fecha_clase')) - dias_laborados)

    @api.onchange('facilitador_id')
    def onchange_facilitador(self):
        if self.facilitador_id:
            return {
                'value': {'tipo_id': self.facilitador_id.tipo_id},
            }
        return {
            'value': {'tipo_id': False},
        }

    @api.constrains('fecha_inicio')
    def check_fecha_inicio(self):
        for record in self:
            if not record.facilitador_id.fecha_ingreso:
                error = _('El facilitador no tiene asignada una fecha de \
                    ingreso. Por favor, contacte al administrador del sistema')
                raise ValidationError(error)
            elif record.fecha_inicio < record.facilitador_id.fecha_ingreso:
                error = _('La fecha inicial del periodo de evaluación no puede \
                    ser menor que la fecha de ingreso del facilitador')
                raise ValidationError(error)
            elif record.fecha_inicio > record.fecha_fin:
                error = _('La fecha inicial del periodo de evaluación no puede \
                    ser mayor a la fecha final')
                raise ValidationError(error)

    @api.constrains('fecha_fin')
    def check_fecha_fin(self):
        for record in self:
            if record.fecha_fin > fields.Date.today():
                error = _('La fecha final del periodo de evaluación no puede \
                    ser mayor a la fecha de hoy')
                raise ValidationError(error)

    @api.multi
    def revision(self):
        self.write({
            'state': 'en_revision',
            'fecha_evaluacion': fields.Date.today()
        })

    @api.multi
    def finalizar(self):
        for record in self:
            if not record.resultado:
                raise ValidationError(_(
                    'Debe indicar un resultado al final de la evaluación'))
        self.write({
            'state': 'finalizada',
            'fecha_evaluacion': fields.Date.today()
        })

    @api.multi
    def set_borrador(self):
        self.write({
            'state': 'borrador',
            'fecha_evaluacion': False,
            'resultado': False,
        })


class EvaluacionPonderacionesRecomendacion(models.Model):
    _name = 'evaluacion.ponderaciones.recomendacion'
    _description = 'Definición de ponderaciones (pesos) de los factores de la \
        evaluacion docente utilizados para determinar los facilitadores \
        recomendados para impartir una clase'
    rec_name = "unidad_id"
    _order = "unidad_id asc"

    MAX_PONDERACION = 5.0

    unidad_id = fields.Many2one(
        'unidad.curricular', 'Unidad Curricular', required=True)

    # Ponderaciones de los factores evaluados del facilitador para realizar
    # recomendacion
    adaptabilidad = fields.Float(
        "Adaptabilidad", digits=(1, 1), required=True, default=0.0)
    seguridad = fields.Float(
        "Seguridad", digits=(1, 1), required=True, default=0.0)
    sociabilidad = fields.Float(
        "Sociabilidad", digits=(1, 1), required=True, default=0.0)
    madurez = fields.Float(
        "Madurez", digits=(1, 1), required=True, default=0.0)
    metas = fields.Float(
        "Metas", digits=(1, 1), required=True, default=0.0)
    liderazgo = fields.Float(
        "Liderazgo", digits=(1, 1), required=True, default=0.0)
    motivacion = fields.Float(
        "Motivación", digits=(1, 1), required=True, default=0.0)
    resultado = fields.Float(
        "Resultado de la evaluación", digits=(1, 1), required=True,
        default=1.0)

    _sql_constraints = [
        ('unique_unidad',
         'unique(unidad_id)',
         _('Solo puede haber un registro por Unidad Curricular.')),
    ]

    def get_factores_evaluados(self):
        return ['adaptabilidad', 'seguridad', 'sociabilidad', 'madurez',
                'metas', 'liderazgo', 'motivacion'] + ['resultado']

    def get_ponderaciones(self, get_defaults=0):
        """
        Devuelve un diccionario con las ponderaciones cada factor evaluado.
        Si get_defaults = True devuelve las ponderaciones por defecto (todos
        con valor 1)
        """
        factores = self.get_factores_evaluados()
        if get_defaults:
            ponderaciones = {factor: 1 for factor in factores}
        else:
            ponderaciones = {
                factor: getattr(self, factor) for factor in factores
            }
        return ponderaciones

    @api.constrains(
        'adaptabilidad', 'seguridad', 'sociabilidad', 'madurez', 'metas',
        'liderazgo', 'motivacion', 'resultado')
    def check_ponderacion(self):
        """
        Garantizar que los valores de los ponderaciones enten entre 0 y
        MAX_PONDERACION
        """
        factores = self.get_factores_evaluados()
        for record in self:
            for factor in factores:
                ponderacion = getattr(record, factor)
                if ponderacion < 0.0 or ponderacion > self.MAX_PONDERACION:
                    raise ValidationError(
                        _("La ponderación debe ser un valor entre 0.0 y %s") %
                        self.MAX_PONDERACION)
