from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class AutoInscripcion(models.TransientModel):
    _name = 'auto.inscripcion'
    _description = 'Asistente para auto-inscripción de alumnos'

    @api.model
    def default_get(self, fields_default):
        res = super(AutoInscripcion, self).default_get(fields_default)
        if 'alumno_id' not in res:
            res['alumno_id'] = self.env['alumno'].search([('user_id', '=', self.env.user.id)], limit=1).id
        alumno = self.env['alumno'].browse(res['alumno_id']).exists()
        if alumno:
            posibles = alumno.mapped('formacion_ids.unidad_ids')
            posibles -= alumno.clase_ids.filtered(lambda c: c.state == 'terminado' and alumno in c.sudo().calificacion_ids.filtered('aprobado').mapped('alumno_id')).mapped('unidad_id')
            posibles -= alumno.clase_ids.filtered(lambda c: c.state != 'terminado').mapped('unidad_id')
            finales = self.env['unidad.curricular']
            for unidad in posibles:
                if unidad.prelacion_ids:
                    if all(prelacion in alumno.calificacion_ids.filtered('aprobado').mapped('unidad_id') for prelacion in unidad.prelacion_ids):
                        finales += unidad
                else:
                    finales += unidad
            if not finales:
                raise ValidationError(_('No tiene unidades curriculares por inscribir.'))
            res['posible_ids'] = [(6, 0, finales.ids)]
        else:
            raise ValidationError(_('Su usuario no está asociado a ningún alumno, verifique con Control Docente.'))
        return res

    alumno_id = fields.Many2one('alumno', 'Alumno')
    calificacion_ids = fields.One2many(related='alumno_id.calificacion_ids')
    formacion_ids = fields.Many2many(related='alumno_id.formacion_ids')
    unidad_ids = fields.Many2many('unidad.curricular', string='Unidades Curriculares')
    posible_ids = fields.Many2many('unidad.curricular', 'posible_unidad_rel', 'posible', 'unidad_id')

    def inscribir(self):
        clases = self.env['clase']
        for unidad in self.unidad_ids:
            clase = clases.sudo().search([('unidad_id', '=', unidad.id), ('state', '=', 'inicio')], limit=1)
            if clase and clase.unidad_id and clase.unidad_id.participantes > len(clase.alumno_ids):
                clase.alumno_ids += self.alumno_id
            else:
                clase = clases.sudo().create({
                    'name': 'Clase de %s / %s' % (unidad.name, fields.Date.today()),
                    'unidad_id': unidad.id,
                    'alumno_ids': [(6, 0, [self.alumno_id.id])]
                })
            clases += clase
        return {
            'type': 'ir.actions.act_window',
            'name': 'Clases inscritas',
            'res_model': 'clase',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'domain': [('id', 'in', clases.ids)],
            'target': 'self'
        }
