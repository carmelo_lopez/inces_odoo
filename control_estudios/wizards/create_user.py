from odoo import fields, models


class CreateUserWizard(models.TransientModel):
    _name = 'create.user.wizard'
    _description = 'Asistente para crear usuario'

    name = fields.Char('Nombre', required=True)
    image = fields.Binary('Foto')
    email = fields.Char('Correo', required=True)
    phone = fields.Char('Teléfono')

    def save_close(self):
        vals = {
            'name': self.name,
            'image': self.image,
            'login': self.email,
            'email': self.email,
            'phone': self.phone,
            'groups_id': [(4, self.env.ref('base.group_user').id)]
        }
        user = self.env['res.users'].create(vals)
        for user_type in ['alumno', 'facilitador', 'empresa']:
            if self.env.context.get('asociar_{}'.format(user_type)):
                user.groups_id = [(4, self.env.ref('control_estudios.group_{}'.format(user_type)).id)]
                person = self.env[user_type].browse(self.env.context['asociar_{}'.format(user_type)]).exists()
                if person:
                    person.user_id = user
        return {'type': 'ir.actions.act_window_close'}
