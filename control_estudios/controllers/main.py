import logging

from odoo import http


class MainController(http.Controller):
    _logger = logging.getLogger(__name__)

    @http.route(['/control_estudios', '/control_estudios/inicio'], auth='none')
    def inicio(self):
        return http.request.render('control_estudios.pagina_inicio', {})
