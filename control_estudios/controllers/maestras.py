"""
Controladores para las rutas de las Maestras (información de cursos y carreras)
Todos los controladores definidos acá deben iniciar con la ruta
`/control_estudios/maestras/`
"""
from odoo import http


class MaestrasController(http.Controller):

    @http.route('/control_estudios/maestras', auth='none')
    def inicio(self):
        modalidades = http.request.env['modalidad'].sudo().search([])
        data = {'tipos': modalidades}
        return http.request.render('control_estudios.inicio_maestras', data)
