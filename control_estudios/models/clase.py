from odoo import _, api, fields, models
from odoo.exceptions import ValidationError

CLASES_STATES = [('inicio', 'Por comenzar'),
                 ('cursando', 'Cursando'),
                 ('terminado', 'Terminado')]


class Calificacion(models.Model):
    _name = 'clase.calificacion'
    _description = 'Calificación final'
    _rec_name = 'alumno_id'

    alumno_id = fields.Many2one('alumno', 'Alumno', required=True)
    clase_id = fields.Many2one('clase', 'Clase')
    formacion_id = fields.Many2one('formacion', related='clase_id.formacion_id')
    unidad_id = fields.Many2one('unidad.curricular', related='clase_id.unidad_id')
    state = fields.Selection(CLASES_STATES, related='clase_id.state')
    calificacion = fields.Float('Calificación', required=True, digits=(2, 1))
    aprobado = fields.Boolean(compute='_compute_aprobado', store=True)
    max_calificacion = fields.Float(related='clase_id.formacion_id.modalidad_id.max_calificacion', readonly=True)
    min_requerido = fields.Float(related='clase_id.formacion_id.modalidad_id.min_requerido', readonly=True)

    _sql_constraints = [
        ('unique_calificacion', 'unique(alumno_id, clase_id)', 'No puede repetir la calificación de un alumno en una misma clase.')
    ]

    @api.depends('calificacion')
    def _compute_aprobado(self):
        for record in self:
            record.aprobado = record.calificacion >= record.min_requerido

    @api.constrains('calificacion')
    def check_calificacion(self):
        for record in self:
            if 0 > record.calificacion > record.max_calificacion:
                raise ValidationError(_('La calificación debe estar entre 0 y %f') % record.max_calificacion)


class Horario(models.Model):
    _name = 'clase.horario'
    _description = 'Horario de clase'
    _order = 'dia asc, hora_inicio asc, hora_fin asc'

    dia = fields.Selection([
        ('0', 'Lunes'),
        ('1', 'Martes'),
        ('2', 'Miércoles'),
        ('3', 'Jueves'),
        ('4', 'Viernes'),
        ('5', 'Sábado'),
        ('6', 'Domingo'),
    ], required=True, help='Día de la semana en que es válido éste horario')
    hora_inicio = fields.Float(required=True)
    hora_fin = fields.Float(required=True)
    name = fields.Char('Nombre')
    active = fields.Boolean(default=True)

    _sql_constraints = [
        ('check_horarios', 'check(hora_inicio < hora_fin)', 'Hora inicio debe ser menor que hora fin.'),
        ('unique_horario', 'unique(dia, hora_inicio, hora_fin)', 'Ya existe éste horario.'),
    ]

    @api.model
    def create(self, vals):
        vals['name'] = '%s %s a %s' % (dict(self._fields['dia'].selection)[vals['dia']], self.formato_hora(vals['hora_inicio']), self.formato_hora(vals['hora_fin']))
        return super(Horario, self).create(vals)

    @api.multi
    def write(self, vals):
        for record in self:
            vals['name'] = '%s %s a %s' % (dict(self._fields['dia'].selection)[vals.get('dia', record.dia)], self.formato_hora(vals.get('hora_inicio', record.hora_inicio)), self.formato_hora(vals.get('hora_fin', record.hora_fin)))
            super(Horario, record).write(vals)
        return True

    @staticmethod
    def formato_hora(hora):
        hora, minuto = map(int, str(float(hora)).split('.'))
        minuto = int(60.0 * float('0.%d' % minuto))
        return '%.2d:%.2d%s' % (hora, minuto, 'am' if hora < 12 else 'pm')


class Clase(models.Model):
    _name = 'clase'
    _description = 'Clase'
    _inherit = ['mail.thread']

    name = fields.Char('Nombre', required=True, track_visibility='onchange')
    active = fields.Boolean(default=True, track_visibility='onchange')
    unidad_id = fields.Many2one('unidad.curricular', 'Unidad Curricular', required=True, track_visibility='onchange')
    formacion_id = fields.Many2one('formacion', related='unidad_id.formacion_id', readonly=True)
    facilitador_id = fields.Many2one('facilitador', 'Facilitador', track_visibility='onchange')
    alumno_ids = fields.Many2many('alumno', string='Alumnos')
    calificacion_ids = fields.One2many('clase.calificacion', 'clase_id', 'Calificaciones')
    state = fields.Selection(string='Estado', default='inicio', required=True, copy=False, track_visibility='onchange',
                             selection=CLASES_STATES)
    horario_ids = fields.Many2many('clase.horario', string='Horarios')
    periodo_id = fields.Many2one('formacion.periodo', 'Periodo', track_visibility='onchange')
    fecha_inicio = fields.Date(related='periodo_id.fecha_inicio')
    fecha_fin = fields.Date(related='periodo_id.fecha_fin')
    calificacion = fields.Float('Calificación', compute='_compute_calificacion')

    def _compute_calificacion(self):
        for clase in self:
            calificaciones = clase.calificacion_ids.filtered(lambda c: c.sudo().alumno_id.user_id == self.env.user)
            clase.calificacion = calificaciones and calificaciones.calificacion

    @api.multi
    def aprobar(self):
        for record in self:
            if record.state != 'inicio':
                raise ValidationError(_('%s debe estar en "Por comenzar" antes de ser aprobado.') % record.name)
            if not record.facilitador_id:
                raise ValidationError(_('Debe asignar un facilitador antes de aprobar esta clase.'))
        self.write({'state': 'cursando', 'fecha_inicio': fields.Date.today()})

    @api.multi
    def terminar(self):
        for record in self:
            if record.state != 'cursando':
                raise ValidationError(_('%s debe estar en "Cursando" antes de ser terminado.') % record.name)
        self.write({'state': 'terminado', 'fecha_fin': fields.Date.today()})

    @api.onchange('unidad_id')
    def _onchange_unidad(self):
        if self.unidad_id:
            vals = {
                'value': {
                    'periodo_id': self.env['formacion.periodo'].search([('modalidad_id', '=', self.formacion_id.modalidad_id.id)], limit=1),
                    'alumno_ids': False
                },
                'domain': {
                    'periodo_id': [('modalidad_id', '=', self.formacion_id.modalidad_id.id)],
                    'alumno_ids': [('state', '=', 'en_proceso')]
                }
            }
            if self.unidad_id.nivel != 1 and self.unidad_id.prelacion_ids:
                clases = self.search([('unidad_id', 'in', self.unidad_id.prelacion_ids.ids), ('state', '=', 'terminado')])
                alumno_ids = clases.mapped('calificacion_ids').filtered('aprobado').mapped('alumno_id.id')
                vals['domain']['alumno_ids'] = [('state', '=', 'en_proceso'), ('id', 'in', alumno_ids)]
            return vals
        return {
            'value': {'periodo_id': False, 'alumno_ids': False},
            'domain': {'periodo_id': [('id', 'in', [0])], 'alumno_ids': [('id', 'in', [0])]}
        }


class Alumno(models.Model):
    _inherit = 'alumno'

    clase_ids = fields.Many2many('clase', 'alumno_clase_rel', 'alumno_id', 'clase_id', 'Clases')
    calificacion_ids = fields.One2many('clase.calificacion', 'alumno_id', 'Calificaciones')
    formacion_ids = fields.Many2many('formacion', string='Formaciones', compute='_compute_formacion_ids')

    def _compute_formacion_ids(self):
        for alumno in self:
            alumno.formacion_ids = alumno.mapped('clase_ids.formacion_id')


class Facilitador(models.Model):
    _inherit = 'facilitador'

    clase_ids = fields.One2many('clase', 'facilitador_id', 'Clases')
    clases_count = fields.Integer('Clases impartidas', compute='_compute_clases_count')
    formacion_ids = fields.Many2many('formacion', string='Formaciones', compute='_compute_formacion_ids')

    def _compute_formacion_ids(self):
        for facilitador in self:
            facilitador.formacion_ids = facilitador.clase_ids.filtered(lambda c: c.state == 'cursando').mapped('formacion_id')

    def _compute_clases_count(self):
        for record in self:
            record.clases_count = len(record.clase_ids.filtered(lambda c: c.state == 'cursando'))
