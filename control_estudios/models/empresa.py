from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
from ..models.persona import Direccion


class Empresa(models.Model, Direccion):
    _name = 'empresa'
    _description = 'Datos Empresas'
    _order = 'deuda desc, rif'
    _inherit = ['mail.thread']

    name = fields.Char('Nombre', required=True, copy=False, track_visibility='onchange')
    logo = fields.Binary(copy=False, attachment=True, track_visibility='onchange')
    rif = fields.Char(required=True, copy=False, track_visibility='onchange')
    obligacion_ids = fields.One2many('empresa.obligacion', 'empresa_id', 'Obligaciones')
    obligaciones_count = fields.Integer('Cantidad de obligaciones', compute='_compute_obligaciones_count')
    alumnos_count = fields.Integer('Cantidad de alumnos', compute='_compute_alumnos_count')
    active = fields.Boolean(default=True)
    user_id = fields.Many2one('res.users', 'Usuario')
    email = fields.Char('Correo', related='user_id.email')
    phone = fields.Char('Teléfono', related='user_id.phone')
    deuda = fields.Integer(copy=False, default=0, track_visibility='onchange')
    cuota = fields.Integer(copy=False, default=0, track_visibility='onchange')
    tipo = fields.Char('Tipo de empresa', track_visibility='onchange')
    contacto_nombre = fields.Char('Nombre', track_visibility='onchange')
    contacto_telefono = fields.Char('Teléfono', track_visibility='onchange')
    contacto_email = fields.Char('Correo', track_visibility='onchange')

    _sql_constraints = [
        ('unique_rif', 'unique(rif)', 'Rif debe ser único')
    ]

    @api.multi
    def _compute_alumnos_count(self):
        for empresa in self:
            empresa.alumnos_count = len(empresa.obligacion_ids.filtered(lambda v: v.alumno_id and v.alumno_id.state == 'en_proceso'))

    @api.multi
    def _compute_obligaciones_count(self):
        for empresa in self:
            empresa.obligaciones_count = len(empresa.obligacion_ids.filtered(lambda v: not v.alumno_id))

    def create_user(self):
        context = self.env.context.copy() if self.env.context else {}
        context.update({
            'default_name': self.name,
            'default_image': self.logo
        })
        return {
            'name': 'Crear usuario',
            'type': 'ir.actions.act_window',
            'res_model': 'create.user.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new',
            'context': context
        }


class EmpresaObligacion(models.Model):
    _name = 'empresa.obligacion'
    _description = 'Obligación de Empresa'
    _rec_name = 'empresa_id'
    _order = 'id desc'

    empresa_id = fields.Many2one('empresa', 'Empresa', required=True, copy=False)
    formacion_id = fields.Many2one('formacion', 'Formación', copy=False)
    alumno_id = fields.Many2one('alumno', 'Alumno', copy=False)
    state = fields.Selection([
        ('vacante', 'Vacante'),
        ('ocupada', 'Ocupada'),
        ('finalizada', 'Finalizada')
    ], 'Estado', default='vacante')
    baremo = fields.Selection([
        ('0', 'Sin calificar'),
        ('1', 'Mal rendimiento'),
        ('2', 'Poco rendimiento'),
        ('3', 'Aceptable'),
        ('4', 'Buen rendimiento'),
        ('5', 'Excelente')
    ], default='0', help='Calificación de la empresa donde el aprendiz cursó pasantías.')

    @api.constrains('empresa_id')
    def _check_cuota_empresa(self):
        for record in self:
            if record.empresa_id and (record.empresa_id.cuota + record.empresa_id.deuda) < len(record.empresa_id.obligacion_ids.filtered(lambda o: o.state == 'ocupada')):
                raise ValidationError(_('La empresa %s no tiene obligaciones pendiente.') % record.empresa_id.name)

    @api.onchange('alumno_id')
    def _onchange_alumno(self):
        for vacante in self:
            vacante.state = 'ocupada' if vacante.alumno_id else 'vacante'

    @api.constrains('alumno_id')
    def _check_alumno(self):
        if self.alumno_id and self.alumno_id.obligacion_id and self.alumno_id.obligacion_id != self:
            raise ValidationError(_('%s ya tiene empresa para pasantías asignada.') % self.alumno_id.nombre_completo)

    @api.onchange('formacion_id', 'empresa_id')
    def _onchange_formacion(self):
        if self.empresa_id:
            aspirantes = self.env['alumno'].search([('state', '=', 'en_proceso'), ('empresa_id', '=', False)])
            escogidos = self.env['alumno']
            if self.formacion_id and self.empresa_id.parish_id:
                escogidos += aspirantes.filtered(lambda a: self.formacion_id in a.formacion_ids and self.empresa_id.parish_id == a.parish_id)
            if not escogidos and self.formacion_id and self.empresa_id.municipality_id:
                escogidos += aspirantes.filtered(lambda a: self.formacion_id in a.formacion_ids and self.empresa_id.municipality_id == a.municipality_id)
            if not escogidos and self.empresa_id.parish_id:
                escogidos += aspirantes.filtered(lambda a: self.empresa_id.parish_id == a.parish_id)
            if not escogidos and self.empresa_id.municipality_id:
                escogidos += aspirantes.filtered(lambda a: self.empresa_id.municipality_id == a.municipality_id)
            # if not escogidos and self.formacion_id:
            #     escogidos += aspirantes.filtered(lambda a: self.formacion_id in a.formacion_ids)
            self.alumno_id = escogidos and escogidos[0] or aspirantes and aspirantes[0]

    @api.multi
    def finalizar(self):
        for vacante in self:
            if vacante.empresa_id.deuda:
                vacante.empresa_id.deuda -= 1
            elif vacante.empresa_id.cuota:
                vacante.empresa_id.cuota -= 1
            vacante.empresa_id.message_post(_('Se finalizó la obligación con el aspirante %s') % vacante.alumno_id.nombre_completo)
        return self.write({'state': 'finalizada'})

    @api.model
    def create(self, vals):
        vacante = super(EmpresaObligacion, self).create(vals)
        if vacante.alumno_id:
            vacante.alumno_id.obligacion_id = vacante
        return vacante

    @api.multi
    def write(self, vals):
        if 'alumno_id' in vals and not vals['alumno_id']:
            for vacante in self:
                vacante.alumno_id.obligacion_id = False
        res = super(EmpresaObligacion, self).write(vals)
        for vacante in self:
            if vacante.alumno_id:
                vacante.alumno_id.obligacion_id = vacante
        return res
