from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class Modalidad(models.Model):
    _name = 'modalidad'
    _description = 'Modalidad de Formación'

    name = fields.Char('Nombre', required=True, copy=False)
    qty_periodos = fields.Integer('Cantidad de Periodos', required=True, default=1)
    tipo_periodo = fields.Selection([('day', 'Diario'),
                                     ('week', 'Semanal'),
                                     ('month', 'Mensual'),
                                     ('trim', 'Trimestral'),
                                     ('sixm', 'Semestral'),
                                     ('year', 'Anual')],
                                    'Tipo de Periodo', required=True, default='sixm',
                                    help='Indica la duración de cada periodo académico')
    active = fields.Boolean('Activo', default=True, copy=False)
    max_calificacion = fields.Float('Calificación máxima', required=True, default=20.0,
                                    help='Modo de calificación, indica la calificación máxima que puede recibir un alumno.')
    min_requerido = fields.Float('Mínimo requerido', required=True, default=9.5,
                                 help='Calificación mímina requerida para aprobar las asignaturas de ésta formacion.')
    pasantias = fields.Boolean('Incluye pasantías')

    _sql_constraints = [
        ('unique_name', 'unique(name)', 'Nombre debe ser único'),
        ('qty_periodos_positive', 'check(qty_periodos > 0)', 'Cantidad de periodos debe ser mayor a 0'),
        ('max_calificacion_positive', 'check(max_calificacion > 0)', 'Calificación máxima debe ser mayor a 0'),
        ('min_requerido_positive', 'check(min_requerido > 0)', 'Mínimo requerido debe ser mayor a 0'),
    ]

    @api.multi
    def write(self, vals):
        if 'qty_periodos' in vals:
            unidades = self.env['unidad.curricular'].search([('formacion_id.modalidad_id', '=', self.id)])
            for unidad in unidades:
                if unidad.nivel > vals['qty_periodos']:
                    raise ValidationError(_('%s en %s es de nivel %s, modifíquelo antes de cambiar los periodos de este tipo de formación.')
                                          % (unidad.name, unidad.formacion_id.name, unidad.nivel))
        return super(Modalidad, self).write(vals)


class UnidadCurricular(models.Model):
    _name = 'unidad.curricular'
    _description = 'Unidad Curricular'
    _order = 'nivel, name'

    name = fields.Char('Nombre', required=True, copy=False)
    codigo = fields.Char('Código', required=True, copy=False)
    formacion_id = fields.Many2one('formacion', 'Formación', required=True)
    active = fields.Boolean('Activo', default=True, copy=False)
    nivel = fields.Integer(required=True, help='Nivel en el que se cursa', default=1)
    prelacion_ids = fields.Many2many('unidad.curricular', 'prelaciones',
                                     'unidad_id', 'prelacion_id', 'Prelaciones', copy=False,
                                     domain='[("nivel", "<", nivel), ("formacion_id", "=", formacion_id)]')
    duracion = fields.Float('Duración', help='Duración en horas', default=0)
    participantes = fields.Integer(help='Cantidad de participantes', default=0)

    _sql_constraints = [
        ('unique_name', 'unique(name, formacion_id)', 'Nombre debe ser único'),
        ('unique_codigo', 'unique(codigo, formacion_id)', 'Código debe ser único')
    ]

    @api.constrains('nivel')
    def check_nivel(self):
        for unidad in self:
            if unidad.nivel < 1 or unidad.nivel > unidad.formacion_id.modalidad_id.qty_periodos:
                raise ValidationError(_('En %s: nivel debe estar entre 1 y %s') % (unidad.name, unidad.formacion_id.modalidad_id.qty_periodos))


class Formacion(models.Model):
    _name = 'formacion'
    _description = 'Formación'
    _order = 'codigo'

    name = fields.Char('Nombre', required=True, copy=False)
    codigo = fields.Char('Código', required=True, copy=False)
    modalidad_id = fields.Many2one('modalidad', 'Modalidad', required=True, copy=True)
    active = fields.Boolean('Activo', default=True, copy=False)
    unidad_ids = fields.One2many('unidad.curricular', 'formacion_id',
                                 string='Unidades Curriculares', copy=False)
    duracion = fields.Float('Duración', compute='_compute_duracion', help='Duración en horas')

    _sql_constraints = [
        ('unique_name', 'unique(name)', 'Nombre debe ser único'),
        ('unique_codigo', 'unique(codigo)', 'Código debe ser único')
    ]

    def _compute_duracion(self):
        for formacion in self:
            formacion.duracion = sum(formacion.mapped('unidad_ids.duracion'))


class FormacionPeriodo(models.Model):
    _name = 'formacion.periodo'
    _description = 'Periodo de una Formacion'
    _order = 'fecha_inicio desc'

    name = fields.Char('Nombre', copy=False, readonly=True)
    modalidad_id = fields.Many2one('modalidad', 'Modalidad', required=True, copy=False)
    fecha_inicio = fields.Date(required=True)
    fecha_fin = fields.Date(required=True)
    active = fields.Boolean('Activo', default=True, copy=False)

    @api.model
    def create(self, vals):
        if 'name' not in vals:
            year = vals.get('fecha_inicio').split('-')[0]
            num = self.search_count([('name', 'like', year), ('modalidad_id', '=', vals.get('modalidad_id'))])
            vals['name'] = '%s/%d' % (year, num + 1)
        return super(FormacionPeriodo, self).create(vals)
