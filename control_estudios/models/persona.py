import base64
from dateutil.relativedelta import relativedelta

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
from odoo.modules import get_module_resource


class Direccion(object):
    """ Clase base para agregar dirección a cualquier modelo """

    @api.model
    def default_country(self):
        return self.env.ref('base.ve').id

    @api.model
    def default_state(self):
        return self.env.ref('l10n_ve_dpt.est_ve_CAR').id

    country_id = fields.Many2one('res.country', 'País', default=default_country)
    state_id = fields.Many2one('res.country.state', 'Estado', default=default_state)
    municipality_id = fields.Many2one('res.country.state.municipality', 'Municipio')
    parish_id = fields.Many2one('res.country.state.municipality.parish', 'Parroquia')
    zip_code = fields.Char('Código postal')
    street = fields.Char('Dirección')

    @api.onchange('country_id')
    def onchange_country(self):
        res = {
            'value': {
                'state_id': self.state_id.country_id == self.country_id and self.state_id.id
            }
        }
        if self.country_id:
            res.update({
                'domain': {
                    'state_id': [('country_id', '=', self.country_id.id)]
                }
            })
        return res

    @api.onchange('state_id')
    def onchange_state(self):
        res = {
            'value': {
                'municipality_id': self.municipality_id.state_id == self.state_id and self.municipality_id.id
            }
        }
        if self.state_id:
            res.update({
                'domain': {
                    'municipality_id': [('state_id', '=', self.state_id.id)]
                }
            })
        return res

    @api.onchange('municipality_id')
    def onchange_municipality(self):
        res = {
            'value': {
                'parish_id': self.parish_id.municipality_id == self.municipality_id and self.parish_id.id
            }
        }
        if self.municipality_id:
            res.update({
                'domain': {
                    'parish_id': [('municipality_id', '=', self.municipality_id.id)]
                }
            })
        return res


class Persona(models.AbstractModel, Direccion):
    _name = 'persona'
    _description = 'Persona'
    _rec_name = 'cedula'
    _order = 'cedula'

    nacionalidad = fields.Selection([('V', 'Venezolano'), ('E', 'Extranjero')], required=True, default='V', copy=False)
    cedula = fields.Char('Cédula', required=True, help='Sin puntos, solo los dígitos, ejemplo 12345678', copy=False)
    primer_nombre = fields.Char(required=True, copy=False)
    segundo_nombre = fields.Char(copy=False)
    primer_apellido = fields.Char(required=True, copy=False)
    segundo_apellido = fields.Char(copy=False)
    nombre_completo = fields.Char(compute='_compute_nombre_completo')
    active = fields.Boolean(default=True, copy=False)
    genero = fields.Selection([('M', 'Masculino'), ('F', 'Femenino')], 'Género')
    fecha_nacimiento = fields.Date(copy=False)
    edad = fields.Integer(compute='_compute_edad')
    foto = fields.Binary(copy=False, attachment=True)
    user_id = fields.Many2one('res.users', 'Usuario')
    email = fields.Char('Correo', related='user_id.email')
    phone = fields.Char('Teléfono', related='user_id.phone')
    etnia = fields.Char()
    estado_civil = fields.Selection([
        ('soltero', 'Soltero'),
        ('casado', 'Casado'),
        ('viudo', 'Viudo'),
        ('divorciado', 'Divorciado'),
        ('concubinato', 'Concubinato')])
    discapacidad = fields.Char()

    _sql_constraints = [
        ('unique_cedula', 'unique(nacionalidad, cedula)', 'Ya existe una persona con la misma cédula.')
    ]

    @api.multi
    def name_get(self):
        return [(record.id, '[{record.nacionalidad}-{record.cedula}] {record.nombre_completo}'.format(record=record)) for record in self]

    @api.constrains('cedula')
    def _check_cedula(self):
        """ Verifica que la cédula tenga el formato correcto, ej: V12345678. """
        cedula = self.cedula.strip().replace('.', '').replace('-', '')
        if not cedula[1:].isdigit():
            raise ValidationError(_('Formato inválido en la cédula, solo debe indicar los dígitos'))
        if 6 > len(cedula) > 8:
            raise ValidationError(_('Verifique la cantidad de dígitos en la cédula, si es menor a 6 agregue 0 a la derecha, y no debe exceder de 8 caracteres.'))

    @api.multi
    @api.depends('primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido')
    def _compute_nombre_completo(self):
        for record in self:
            record.nombre_completo = ' '.join(name for name in (record.primer_nombre, record.segundo_nombre, record.primer_apellido, record.segundo_apellido) if name)

    @api.multi
    @api.depends('fecha_nacimiento')
    def _compute_edad(self):
        for record in self:
            record.edad = relativedelta(*map(fields.Date.from_string, (fields.Date.today(), record.fecha_nacimiento))).years

    @api.model
    def _get_default_image(self, gender):
        image = False
        if gender == 'M':
            img_path = get_module_resource('control_estudios', 'static/src/img', 'male.png')
        else:
            img_path = get_module_resource('control_estudios', 'static/src/img', 'female.png')
        if img_path:
            with open(img_path, 'rb') as img_file:
                image = img_file.read()
        return base64.b64encode(image)

    @api.model
    def create(self, vals):
        if not vals.get('foto'):
            vals['foto'] = self._get_default_image(vals.get('genero', 'M'))
        vals['cedula'] = vals.get('cedula', '').strip().replace('.', '')
        return super(Persona, self).create(vals)

    @api.multi
    def write(self, vals):
        res = super(Persona, self).write(vals)
        male_pic = self._get_default_image('M')
        female_pic = self._get_default_image('F')
        for persona in self.filtered('genero'):
            if not persona.foto or persona.foto == (male_pic if persona.genero == 'F' else female_pic):
                persona.foto = male_pic if persona.genero == 'M' else female_pic
        if 'cedula' in vals:
            vals['cedula'] = vals['cedula'].strip().replace('.', '')
        return res

    def create_user(self):
        context = self.env.context.copy() if self.env.context else {}
        context.update({
            'default_name': '{self.primer_nombre} {self.primer_apellido}'.format(self=self),
            'default_image': self.foto
        })
        return {
            'type': 'ir.actions.act_window',
            'name': 'Crear usuario',
            'res_model': 'create.user.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'context': context,
            'target': 'new'
        }
