from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class Alumno(models.Model):
    _name = 'alumno'
    _description = 'Alumno'
    _inherit = 'persona'

    state = fields.Selection([
        ('aspirante', 'Aspirante'),
        ('en_proceso', 'En proceso'),
        ('retirado', 'Retirado'),
        ('egresado', 'Egresado')
    ], default='aspirante', copy=False, required=True)
    obligacion_id = fields.Many2one('empresa.obligacion', 'Obligación')
    empresa_id = fields.Many2one('empresa', 'Empresa', related='obligacion_id.empresa_id', store=True)
    baremo = fields.Selection(related='obligacion_id.baremo')

    @api.multi
    def set_en_proceso(self):
        for alumno in self:
            if alumno.state not in ['aspirante', 'retirado']:
                raise ValidationError(_('Alumno debe tener estado "Aspirante" o "Retirado" antes de poder marcarlo como "En proceso".'))
            alumno.state = 'en_proceso'

    @api.multi
    def set_aspirante(self):
        for alumno in self:
            if alumno.state != 'en_proceso':
                raise ValidationError(_('Alumno debe tener estado "En proceso" antes de poder marcarlo como "Aspirante".'))
            alumno.state = 'aspirante'

    @api.multi
    def set_retirado(self):
        for alumno in self:
            if alumno.state != 'en_proceso':
                raise ValidationError(_('Alumno debe tener estado "En proceso" antes de poder marcarlo como "Retirado".'))
            alumno.state = 'egresado'

    @api.multi
    def set_egresado(self):
        for alumno in self:
            if alumno.state != 'en_proceso':
                raise ValidationError(_('Alumno debe tener estado "En proceso" antes de poder marcarlo como "Egresado".'))
            alumno.state = 'egresado'
