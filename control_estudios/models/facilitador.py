from odoo import models


class Facilitador(models.Model):
    _name = 'facilitador'
    _description = 'Personal que dicta los cursos'
    _inherit = 'persona'
