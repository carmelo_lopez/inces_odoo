import logging

from odoo.addons.control_estudios.tests.common import CommonCase
from odoo.tools.profiler import profile

_logger = logging.getLogger(__name__)


class TestAsignacionEmpresa(CommonCase):

    def test_01_empresas_sin_obligaciones(self):
        for empresa in self.empresas:
            self.assertEqual(len(empresa.obligacion_ids), 0, '%s tiene vacantes' % empresa.name)

    def test_02_crear_obligaciones(self):
        empresas = self.empresas
        empresas.write({
            'obligacion_ids': [(0, 0, {}) for i in range(20)]
        })
        for empresa in empresas:
            self.assertEqual(len(empresa.obligacion_ids), 20, '%s tiene %d vacantes de 20' % (empresa.name, len(empresa.obligacion_ids)))

    @profile
    def test_03_asignar_obligaciones(self):
        formaciones = self.formaciones
        empresas = self.empresas
        # Asignamos 20 veces la formacion a cada empresa
        for empresa in empresas:
            for i, obligacion in enumerate(empresa.obligacion_ids):
                obligacion.formacion_id = formaciones[i % 5]
                # Con la formacion asignada, llamamos el algoritmo
                obligacion._onchange_formacion()
        self.assertTrue(all(bool(o.formacion_id) for o in empresas.mapped('obligacion_ids')))
        obligaciones_ocupadas = empresas.mapped('obligacion_ids').filtered(lambda o: o.alumno_id)
        _logger.info('Obligaciones ocupadas: %d/1000', len(obligaciones_ocupadas))
        aspirantes_asignados = self.alumnos.filtered(lambda a: a.empresa_id)
        self.assertEqual(len(obligaciones_ocupadas), len(aspirantes_asignados), 'No se asignaron correctamente todas las obligaciones.')
