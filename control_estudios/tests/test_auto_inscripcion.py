import logging
import random

from odoo.addons.control_estudios.tests.common import CommonCase
from odoo.tools.profiler import profile
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class TestAutoInscripcion(CommonCase):

    def test_01_clases_completas(self):
        for clase in self.clases:
            self.assertEqual(len(clase.alumno_ids), 40, '%s no tiene 40 alumnos' % clase.name)

    def test_02_clases_aprobadas(self):
        for clase in self.clases:
            clase.aprobar()
            self.assertTrue(clase.state == 'cursando', 'La clase %s no pudo ser aprobada' % clase.name)

    def test_03_calificar_alumnos(self):
        for clase in self.clases:
            clase.calificacion_ids = [(0, 0, {
                'alumno_id': alumno.id,
                'calificacion': float(random.randrange(5, 20))
            }) for alumno in clase.alumno_ids]
            clase.terminar()
            self.assertTrue(clase.state == 'terminado', 'La clase %s no se pudo finalizar' % clase.name)
            self.assertEqual(len(clase.calificacion_ids), len(clase.alumno_ids), 'No se evaluaron todos los alumnos en la clase %s' % clase.name)
        _logger.info('Todos los alumnos han sido calificados, han aprobado %d de %d', len(self.clases.mapped('calificacion_ids').filtered(lambda c: c.aprobado)), len(self.clases.mapped('calificacion_ids')))

    @profile
    def test_04_auto_inscripcion(self):
        # Inactivamos el periodo en curso
        self.periodos[0].active = False
        clases_nuevas = []

        auto_inscripcion = self.env['auto.inscripcion']
        for alumno in self.alumnos:
            try:
                inscripcion = auto_inscripcion.with_context(default_alumno_id=alumno.id).create({})
            except ValidationError:
                continue
            inscripcion.unidad_ids = inscripcion.posible_ids
            clases_nuevas += inscripcion.inscribir().get('domain', [(0, 0, [])])[0][2]
        clases_nuevas = self.env['clase'].browse(clases_nuevas).exists()
        _logger.info('Alumnos inscritos: %d, clases creadas: %d', len(clases_nuevas.mapped('alumno_ids')), len(clases_nuevas))
