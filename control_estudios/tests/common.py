from datetime import date, timedelta

from odoo.tests.common import SingleTransactionCase


class CommonCase(SingleTransactionCase):
    """ Este test servirá como base para la configuración de Control de Estudios. """

    @property
    def municipios(self):
        return self.env['res.country.state.municipality'].search([('name', 'ilike', 'Municipio Prueba')])

    @property
    def parroquias(self):
        return self.env['res.country.state.municipality.parish'].search([('name', 'ilike', 'Parroquia Prueba')])

    @property
    def alumnos(self):
        return self.env['alumno'].search([('primer_nombre', '=', 'Test Alumno')])

    @property
    def facilitadores(self):
        return self.env['facilitador'].search([('primer_nombre', '=', 'Test Facilitador')])

    @property
    def empresas(self):
        return self.env['empresa'].search([('name', 'ilike', 'Test Empresa')])

    @property
    def formaciones(self):
        return self.env['formacion'].search([('name', 'ilike', 'Test Formacion')])

    @property
    def periodos(self):
        return self.env['formacion.periodo'].search([('name', 'ilike', 'Test')])

    @property
    def clases(self):
        return self.env['clase'].search([('name', 'ilike', 'Clase Test de')])

    @property
    def modalidad(self):
        return self.env['modalidad'].search([('name', '=', 'Curso Test Prueba')], limit=1)

    def test_00_preparar_data(self):
        self.crear_municipios()
        self.crear_parroquias()
        self.crear_alumnos()
        self.crear_facilitadores()
        self.crear_empresas()
        self.crear_modalidad()
        self.crear_formaciones()
        self.crear_periodos()
        self.crear_clases()

    def crear_municipios(self):
        """ Creamos 5 municipios ficticios dentro del estado Carabobo. """
        carabobo = self.env.ref('l10n_ve_dpt.est_ve_CAR')
        municipios = self.env['res.country.state.municipality']
        for i in range(1, 6):
            municipios += municipios.create({
                'state_id': carabobo.id,
                'code': 'MUN_TEST_%d' % i,
                'name': 'Municipio Prueba %d' % i
            })
        return municipios

    def crear_parroquias(self):
        """ Creamos 50 parroquias ficticios dentro del estado Carabobo. """
        parroquias = self.env['res.country.state.municipality.parish']
        for i, municipio in enumerate(self.municipios, 1):
            for j in range(1, 11):
                parroquias += parroquias.create({
                    'municipality_id': municipio.id,
                    'code': 'PAR_TEST_%s_%d' % (i, j),
                    'name': 'Parroquia Prueba %d:%d' % (i, j)
                })
        return parroquias

    def crear_alumnos(self):
        """ Crearemos al menos 1000 alumnos, 30 con discapacidad motriz, 40 con
        discapacidad sensorial y 70 de etnia wayú. 500 de cada género,
        repartidos entre 5 municipios y 50 parroquias. """
        alumnos = self.env['alumno']
        parroquias = self.parroquias
        for i in range(1, 1001):
            parroquia = parroquias[(i - 1) % 50]
            alumnos += alumnos.create({
                'primer_nombre': 'Test Alumno',
                'primer_apellido': '%.3d' % i,
                'cedula': '20000%.3d' % i,
                'genero': 'F' if i % 2 == 0 else 'M',
                'fecha_nacimiento': (date(1990, 1, 1) + timedelta(days=i * 3)).strftime('%Y-%m-%d'),
                'estado_civil': 'soltero',
                'discapacidad': 'motriz' if i % 27 == 0 else 'sensorial' if i % 25 == 0 else None,
                'etnia': 'Wayú' if i % 14 == 0 else None,
                'state': 'en_proceso',
                'municipality_id': parroquia.municipality_id.id,
                'parish_id': parroquia.id
            })
        return alumnos

    def crear_facilitadores(self):
        """ Ahora crearemos al menos a 20 facilitadores. """
        facilitadores = self.env['facilitador']
        for i in range(1, 21):
            facilitadores += facilitadores.create({
                'primer_nombre': 'Test Facilitador',
                'primer_apellido': '%.2d' % i,
                'cedula': '120000%.2d' % i,
                'genero': 'F' if i % 2 == 0 else 'M',
                'fecha_nacimiento': (date(1970, 1, 1) + timedelta(days=i * 30)).strftime('%Y-%m-%d'),
                'estado_civil': 'soltero',
                'discapacidad': 'motriz' if i % 7 == 0 else 'sensorial' if i % 25 == 0 else None,
                'etnia': 'Wayú' if i % 15 == 0 else None,
            })
        return facilitadores

    def crear_empresas(self):
        """ Generamos data para unas 50 empresas. Repartidas entre 5 municipios
        y 50 parroquias. """
        empresas = self.env['empresa']
        parroquias = self.parroquias
        for i in range(1, 51):
            parroquia = parroquias[i % 50]
            empresas += empresas.create({
                'name': 'Test Empresa %.2d' % i,
                'rif': 'J-300000%.2d-1' % i,
                'municipality_id': parroquia.municipality_id.id,
                'parish_id': parroquia.id
            })
        return empresas

    def crear_modalidad(self):
        """ Creamos la modalidad de curso (solo basta una para las pruebas). """
        modalidad = self.env['modalidad'].create({
            'name': 'Curso Test Prueba',
            'tipo_periodo': 'trim',
            'qty_periodos': 6,
            'pasantias': True,
        })
        return modalidad

    def crear_formaciones(self):
        """ Generamos 5 formaciones con sus respectivas unidades curriculares. """
        formaciones = self.env['formacion']
        for i in range(1, 6):
            formacion = formaciones.create({
                'name': 'Test Formacion %d' % i,
                'codigo': 'FORM_TEST_%d' % i,
                'modalidad_id': self.modalidad.id,
                'unidad_ids': [(0, 0, {
                    'name': 'Unidad Curricular %d:%d' % (i, j),
                    'codigo': 'UC_TEST_%d_%d' % (i, j),
                    'nivel': int(float(j + 1) / 2),
                }) for j in range(1, 13)]
            })
            formaciones += formacion
            # Acomodamos las prelaciones de cada formacion
            for j, un_cur in enumerate(formacion.unidad_ids[2:], 2):
                un_cur.prelacion_ids = formacion.unidad_ids[j - 2]
        return formaciones

    def crear_periodos(self):
        """ Generamos 6 periodos para la modalidad de curso que creamos previamente. """
        periodos = self.env['formacion.periodo']
        for i in range(6):
            periodo = periodos.create({
                'modalidad_id': self.modalidad.id,
                'fecha_inicio': (date.today() + timedelta(days=i * 90)).strftime('%Y-%m-%d'),
                'fecha_fin': (date.today() + timedelta(days=i * 90) + timedelta(days=90)).strftime('%Y-%m-%d')
            })
            periodo.name = 'Test %s' % periodo.name
            periodos += periodo
        return periodos

    def crear_clases(self):
        """ Se crearán 10 clases por cada formación, 5 clases para cada unidad
        curricular del primer nivel, con 40 alumnos cada una, para un total
        de 50 clases y los 1000 alumnos con 2 clases cada uno. """
        clases = self.env['clase']
        for f, formacion in enumerate(self.formaciones):
            for i in range(5):
                for unidad in formacion.unidad_ids[:2]:
                    clases += clases.create({
                        'name': 'Clase Test de %s, seccion %d, periodo %s' % (unidad.name, i + 1, self.periodos[0].name),
                        'unidad_id': unidad.id,
                        'facilitador_id': self.facilitadores[len(self.clases) % 20].id,
                        'alumno_ids': [(4, alumno.id) for alumno in self.alumnos[((f * 5) + i) * 40:(((f * 5) + i) * 40) + 40]]
                    })
        return clases
