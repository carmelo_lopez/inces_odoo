# Control de Estudios INCES

Módulo para la gestión de Control de Estudios en la institución INCES.

## Autores

- Jissel Rodríguez, [jisselrh](https://gitlab.com/jisselrh)
- Carmelo López, [carmelo-lopez](https://gitlab.com/carmelo_lopez)
