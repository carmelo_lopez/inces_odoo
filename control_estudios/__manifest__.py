# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Control de Estudios INCES',
    'version': '12.0.1.0.0',
    'author': 'FaCyT',
    'maintainer': 'FaCyT',
    'website': 'https://gitlab.com/carmelo_lopez/inces_odoo',
    'license': 'AGPL-3',
    'category': 'Extra Tools',
    'summary': 'Gestión académica del INCES',
    'depends': ['mail', 'l10n_ve_dpt'],
    'data': [
        # Grupos Seguridad
        'security/groups.xml',
        # Vistas comunes
        'views/main.xml',
        'views/formacion.xml',
        'views/alumno.xml',
        'views/facilitador.xml',
        'views/clase.xml',
        'views/empresa.xml',
        'wizards/create_user.xml',
        'wizards/auto_inscripcion.xml',
        # Reglas de acceso
        'security/ir.model.access.csv',
        # Reglas de registro
        'security/ir.rule.xml',
        # Páginas de los controllers
        'static/src/xml/layout.xml',
        'static/src/xml/inicio.xml',
        'static/src/xml/maestras/inicio_maestras.xml',
        # Reportes
        'reports/alumno.xml',
        'reports/facilitador.xml',
        'reports/clase.xml',
        'reports/empresa.xml',
    ],
    'installable': True,
    'application': True,
    'demo': [
        'demo/horarios.xml',
    ],
}
