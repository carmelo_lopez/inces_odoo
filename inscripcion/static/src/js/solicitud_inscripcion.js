odoo.define('inscripcion.js_inscripcion', function(require) {
'use strict';

console.log('-----------------------------------------------------------');


var frm = $('#form_enviarSolicitud');
var frm_aspirante = $('#form_enviarSolicitudAspirante');

var ajax = require('web.ajax');
var horarios_ids = []
var habilidades_elegidas = [];
var modalidades_elegidas = [];

var session = require('web.session');
require('root.widget');


    frm.submit(function (e) {
        e.preventDefault();

        ajax.jsonRpc("/inscripcion/enviar_solicitud", 'call', {
            idn: $("input[name='idn']").val(),
            name: $("input[name='name']").val(),
            input_tipo: $("input[name='input_tipo']").val(),
            input_periodo: $("input[name='input_periodo']").val(),
            input_actividad: $("input[name='input_actividad']").val(),
            input_horario: $("input[name='input_horario']").val(),
            tipo_solicitante: $("input[name='tipo_solicitante']").val(),


        }).then(function (data) {
            console.log(data);
            if (data.status === 'done') {
                $('enviarSolcitud').attr('class', 'panel panel-success');
                $('enviarSolcitud').text('Solicitud Recibida').addClass('bg-success');
                alert('Solicitud Recibida');

                /*showMessage(myMessages[3]);
                $('.message').click(function(){              
                          $(this).animate({top: -$(this).outerHeight()}, 500);
                  });
                */
            }
            else {
                alert('Hubo un error Procesando Solicitud \n' + data.status);
            }
        });
  });



    frm_aspirante.submit(function (e) {
        e.preventDefault();
        var data_input =  frm_aspirante.serializeArray();
        var data = {};
        var data_input_form = frm.serializeArray();
        var data_solicitud = {};

        $(data_input).each(function(index, obj){
            data[obj.name] = obj.value;
        });

        $(data_input_form).each(function(index, obj){
            data_solicitud[obj.name] = obj.value;
        });


        ajax.jsonRpc("/inscripcion/formulario_aspirante", 'call', {
          data,data_solicitud
        }).then(function (data) {
            console.log(data);
            if (data.status === 'done') {
                $('enviarSolcitud').attr('class', 'panel panel-success');
                $('enviarSolcitud').text('Solicitud Recibida').addClass('bg-success');
                alert('Solicitud Recibida');
                var url = "/inscripcion/solicitudes_historial/"+data.res.aspirante+"/"+data.res.solicitud;    
                $(location).attr('href',url);
                /*showMessage(myMessages[3]);
                $('.message').click(function(){              
                          $(this).animate({top: -$(this).outerHeight()}, 500);
                  });
                */
            }
            else {
                alert('Hubo un error Procesando Solicitud \n' + data.status);
            }
        });
  });

    $('#participanteModal #idn').change(function(e){
        var cedula =  $(this).val();
        console.warn(e);
        ajax.jsonRpc("/inscripcion/get_cedula", 'call', {
          cedula:cedula,
        }).then(function (data) {
            console.log(data);
            if (data.status === 'done') {

                $.each(data.res , function (index, value) {
                    console.log("aspirante   : " + value + " " + index);
                    $("#"+index).val(value);
                    //$(index).text(value);
                      });

            }
            else {
                console.log('Hubo un error Procesando Solicitud \n' + data.status);
                //alert('Hubo un error Procesando Solicitud \n' + data.status);
            }
        });
    });

    $('.formacion_button').click(function(e){
        var formacion =  $(this).attr('id');
        console.warn($(this).val());
        console.warn(e);
        ajax.jsonRpc("/inscripcion/get_formacion_informacion", 'call', {
          formacion:formacion,
        }).then(function (data) {
            console.log(data);
            if (data.status === 'done') {

                    console.log("aspirante   : " + data.res.formacion + " " + data.res.descripcion);
                    //$("#"+index).val(value);
                    //$(index).text(value);
                      
                      $('#titulo_formacion').empty();
                      $('#foto_formacion').attr("src","data:image/png;base64,"+data.res.foto)
                      $("#titulo_formacion_card").html("<b>" + data.res.formacion + "<b>");
                      $("#subtitulo_formacion").html( "<b>" + data.res.formacion_subtitulo + "<b>" 
                          + "<br> <b> Habilidad Requerida: " + data.res.habilidad_requerida + "</b>" 
                          + "<br> <b> Modalidad:" + data.res.modalidad + " </b> <br>"
                          + "<br> <b>Duración: </b>" + data.res.duracion + " Horas <br>"
                          );


                      $("<p></p>", {
                          'text':data.res.formacion,
                          'id':'descripcion_formacion',
                          'html': $("<p></p",{
                                'class': 'actividades',
                                'text' : data.res.descripcion,
                                }),                          
                        }
                      ).appendTo('#subtitulo_formacion');

      
            }
            else {
                console.log('Hubo un error Procesando Solicitud \n' + data.status);
                //alert('Hubo un error Procesando Solicitud \n' + data.status);
            }
        });
    });


    $('.habilidad_button').mouseleave(function(e){
        var formacion =  $(this).attr('id');
        habilidades_elegidas = [];
        modalidades_elegidas = [];        

          $('.modalidad_button.active').each(function() {
               console.log(this.value);
                console.log(modalidades_elegidas);
               modalidades_elegidas.push(this.id);
          });
         if($(this).is(':active')){
                //modalidades_elegidas.push(this.id);
            }
        console.log("modalidades ::: <<<<<<<<<<<" + modalidades_elegidas);

            //$('.habilidad_button').removeClass('active')
            $('#formacionesDiv').empty();

          //$('.habilidad_button.active').each(function() {
                //this.removeClass('active')
                //console.log(this.value);
                //console.log(habilidades_elegidas);
               //habilidades_elegidas.push(this.value);
          //});
        //console.log("habilidades ::: <<<<<<< " +habilidades_elegidas);


    });



    $('.modalidad_button').mouseleave(function(e){
        var formacion =  $(this).attr('id');
        habilidades_elegidas = [];
        modalidades_elegidas = [];        

          $('.modalidad_button.active').each(function() {
               console.log(this.value);
                console.log(modalidades_elegidas);
               modalidades_elegidas.push(this.id);
          });

        console.log("modalidades :::>>>>" + modalidades_elegidas);



          $('.habilidad_button.active').each(function() {
               console.log(this.value);
                console.log(habilidades_elegidas);
               habilidades_elegidas.push(this.value);
          });
         if($(this).is(':active')){
                //habilidades_elegidas.push(this.value);
            }
        console.log("habilidades :::>>>> " +habilidades_elegidas);


        ajax.jsonRpc("/inscripcion/get_actividades", 'call', {
          habilidades_elegidas:habilidades_elegidas,
          modalidades_elegidas:modalidades_elegidas,
          }).then(function (data) {
            console.log(data);
            if (data.status === 'done') {

                $('#formacionesDiv').empty();
                $.each(data.res.objeto , function (index, formacion) {

                  $("<input></input>", {
                      'type':"button",
                      'class': "btn btn-outline-primary mb-3 p-2  ml-2 formacion_button",
                      'text':formacion,
                      'value':formacion,
                       "on": {

                              "click": function( event ) {


                                    var formacion =  $(this).val();
                                    console.warn($(this).val());
                                    console.warn(e);
                                    ajax.jsonRpc("/inscripcion/get_formacion_informacion", 'call', {
                                      formacion:formacion,
                                    }).then(function (data) {
                                        console.log(data);
                                        if (data.status === 'done') {

                                                console.log("aspirante   : " + data.res.formacion + " " + data.res.descripcion);
                                                //$("#"+index).val(value);
                                                //$(index).text(value);
                                                  
                                                  $('#titulo_formacion').empty();
                                                  $('#foto_formacion').attr("src","data:image/png;base64,"+data.res.foto)
                                                  $("#titulo_formacion_card").html("<b style='padding:15px; '>" + data.res.formacion + "<b>");
                                                  $("#subtitulo_formacion").html( "<b>" + data.res.formacion_subtitulo + "<b>" + "<br> <b> Modalidad: </b>" + data.res.modalidad + "" + "<br> <b> Habilidad Requerida: </b>" + data.res.habilidad_requerida + "" + "<br><b>Duración: </b>" + data.res.duracion + " Horas");
                                                  
                                                  $("#input_tipo").val(data.res.modalidad);
                                                  $("#input_actividad").val(data.res.formacion);

                                                  $("<p></p>", {
                                                      'text':data.res.formacion,
                                                      'id':'descripcion_formacion',
                                                      'html': $("<p></p",{
                                                            'class': 'actividades',
                                                            'text' : data.res.descripcion,
                                                            }),                          
                                                    }
                                                  ).appendTo('#subtitulo_formacion');

                                                  //$("<button></button>", {
                                                  //    'text':"Realizar Solicitud de Inscripción",
                                                  //    'class': 'btn btn-primary btn-lg btn-block',
                                                  //    'id':'boton_procesar',
                                                  //    'on':{
                                                  //        "click": function( event ) {
                                                  //          alert("Submit");
                                                  //        }

//                                                     }
  //                                                  }
    //                                              ).appendTo('#subtitulo_formacion');

                                  



                                        }
                                        else {
                                            console.log('Hubo un error Procesando Solicitud \n' + data.status);
                                            //alert('Hubo un error Procesando Solicitud \n' + data.status);
                                        }
                                    });


                              }

                       }
                    }).appendTo('#formacionesDiv');

                });


            }
            else {
                console.log('Hubo un error Procesando Solicitud \n' + data.status);
                //alert('Hubo un error Procesando Solicitud \n' + data.status);
            }
        });
    });




/////////////////////////////Evento tipo de actividad //////////////////////////
          $(".tipo-items").click(function(){

            //alert("Tipowas clicked.");
              $('#collapseActividades').collapse('show');
              $('#collapsePeriodos').collapse('hide');
              $('#collapseHorario').collapse('hide');
              $('#cardCollapseActividades').addClass("border-warning");
              /* Agregando valor al div del formulario*/ 
              $('#tipo_act').html($(this).text());
              
              $('#cardCollapseFIRST').removeClass("border-light");
              $('#cardCollapseFIRST').removeClass("border-warning");
              $('#cardCollapseActividades').removeClass("border-light");
              $('#cardCollapseActividades').addClass("border-warning");


              $('#cardCollapseFIRST').addClass("border-success");
              $('#collapseFIRST').collapse("toggle");
              // remove a class


              $('#tipo_act').empty();
              $("<span></span>", {
                  'class':'badge badge-success',
                  'text':$(this).text(),
                  }
              ).appendTo('#tipo_act')

              //$('#periodo_act').html('Selecciona un Periodo');
              $('#curso_act').empty();
              $("<span></span>", {
                  'class':'badge badge-warning',
                  'text':'Selecciona una Actividad',
                  'id':'periodo_act_text',
                  }
              ).appendTo('#curso_act');

              //colapsando seleccion actual:
              //('#collapseActividades').collapse('hide')

              $('#periodo_act').html('');
              $('#horario_act').html('');
              $("#enviarSolcitud").prop('disabled', true);
              $("#input_tipo").val($(this).children('.tipo').text());
              $("#input_periodo").val('');
              $("#input_actividad").val('');
              $("#input_horario").val('');


              ajax.jsonRpc("/inscripcion/get_actividades", 'call', {
                  input_tipo: $("input[name='input_tipo']").val(),
                  habilidades_filtro: $("input[name='habilidades_filtro']").val(),
              }).then(function (data) {
                  console.log(data);  
                  console.log("sstatus " + data.status);

                  if (data.status === 'done') {
                      //alert(data);
                      //alert('Solicitud Recibida');
                      //$('#contenedorPeriodos').
                      //cambios////


                      //cambios////
                      $('#contenedorActividades').children("a").remove();
                      $.each(data.res.objeto , function (index, actividad) {
                          console.log("actividad   : " + actividad);
                          $("<a></a>", {
                              'class': "list-group-item actividad-items",
                              'html': $("<h4></h4",{
                                    'class': 'actividades',
                                    'text' : actividad,
                                    }),

                              //'href' : "#collapsePeriodos",
                              "on": {
                                  // attach `click` event to dynamically created element
                                  //Definiendo el On Click de Periodos, desencadena Cursos
                                  //y via ajax se consultan los horarios

                                      "click": function( event ) {
                                        // Do something
                                          $('#collapsePeriodos').collapse('show');
                                          $('#collapseActividades').collapse("toggle");
                                          $('#collapseHorario').collapse('hide');
                                          $('#cardCollapseActividades').removeClass("border-warning");
                                          $('#cardCollapseActividades').addClass("border-success");
                                          $('#cardCollapsePeriodos').removeClass("border-light");
                                          $('#cardCollapsePeriodos').addClass("border-warning");




                                          /* Agregando valor al div del formulario*/ 
                                          //$('#periodo_act').html($(this).text());
                                          $('#curso_act').empty();
                                          $("<span></span>", {
                                              'class':'badge badge-success',
                                              'text':$(this).text(),
                                              }
                                          ).appendTo('#curso_act')


                                          //$('#curso_act').html('Selecciona una Actividad');
                                          $('#periodo_act').empty();
                                          $("<span></span>", {
                                              'class':'badge badge-warning',
                                              'text':'Selecciona un Periodo',
                                              'id':'curso_act_text',
                                              }
                                          ).appendTo('#periodo_act')

                                          //$('#horario_act').html('');
                                          //$("#enviarSolcitud").prop('disabled', true);
                                          $("#input_actividad").val($(this).children('.actividades').text());
                                          $("#input_periodo").val('');
                                          $("#input_horario").val('');


                                            ajax.jsonRpc("/inscripcion/get_periodos", 'call', {
                                                input_tipo: $("input[name='input_tipo']").val(),
                                                input_actividad: $("input[name='input_actividad']").val(),   

                                            }).then(function (data) {
                                                console.log(data);  
                                                console.log("sstatus " + data.status);

                                                if (data.status === 'done') {
                                                    //alert(data);
                                                    //alert('Solicitud Recibida');
                                                    //$('#contenedorPeriodos').
                                                      $('#contenedorPeriodos').children("a").remove();
                                                      $.each(data.res.objeto , function (index, periodo) {
                                                        console.log("Periodo   : " + periodo);
                                                          $("<a></a>", {
                                                                'class': "list-group-item periodo-items",
                                                                'html': $("<h4></h4",{
                                                                      'class': 'periodo',
                                                                      'text' : periodo,
                                                                      }),

                                                                //'href' : "#collapseHorario",
                                                                "on": {
                                                                        // attach `click` event to dynamically created element
                                                                        //Definiendo el On Click de Actividades, desencadena Horarios
                                                                        //y via ajax se consultan los horarios
                                                                        "click": function( event ) {

                                                                            $('#collapseHorario').collapse('show');
                                                                            $('#collapsePeriodos').collapse("toggle");
                                                                            $('#cardCollapseHorario').removeClass("border-light");
                                                                            $('#cardCollapsePeriodos').removeClass("border-warning");
                                                                            $('#cardCollapsePeriodos').addClass("border-success");
                                                                            //$('#cardCollapsePeriodos').collapse("hide"); 
                                                                            $('#cardCollapseHorario').removeClass("border-light");
                                                                            $('#cardCollapseHorario').addClass("border-warning");


                                                                                                                                                       /* Agregando valor al div del formulario*/ 
                                                                            //$('#curso_act').html($(this).text())
                                                                            //$('#horario_act').html('Selecciona un Horario');
                                                                              $('#periodo_act').empty();
                                                                              $("<span></span>", {
                                                                                  'class':'badge badge-success',
                                                                                  'text':$(this).text(),
                                                                                  }
                                                                              ).appendTo('#periodo_act')


                                                                              $('#horario_act').empty();

                                                                              $("<span></span>", {
                                                                                  'class':'badge badge-warning',
                                                                                  'text':'Selecciona un Horario',
                                                                                  'id':'horario_act_text',
                                                                                  }
                                                                              ).appendTo('#horario_act')          

                                                                              $("#input_periodo").val($(this).children('.periodo').text());
                                                                              $("#input_horario").val('');


                                                                                      ajax.jsonRpc("/inscripcion/get_horario", 'call', {
                                                                                          input_tipo: $("input[name='input_tipo']").val(),
                                                                                          input_periodo: $("input[name='input_periodo']").val(),
                                                                                          input_actividad: $("input[name='input_actividad']").val(),

                                                                                      }).then(function (data) {
                                                                                          console.log(data);  
                                                                                          console.log("status " + data.status);

                                                                                          if (data.status === 'done') {
                                                                                              //alert(data);
                                                                                              //alert('Solicitud Recibida');
                                                                                              //$('#contenedorPeriodos').
                                                                                              $('#contenedorHorario').children("a").remove();
                                                                                              $.each(data.res.objeto , function (index, horario) {
                                                                                                  $(this).addClass("active");
                                                                                                  console.log("actividades   : " + horario);

                                                                                                  $("<a></a>", {
                                                                                                      'class': "list-group-item actividades-items",
                                                                                                      'html': $("<h4></h4",{
                                                                                                            'class': 'horario',
                                                                                                            'text' : horario,
                                                                                                            }),
                                                                                                      //'href' : "#",

                                                                                                      "on": {
                                                                                                              // attach `click` event to dynamically created element
                                                                                                              "click": function( event ) {
                                                                                                              $('#cardCollapseHorario').removeClass("border-warning");
                                                                                                              $('#cardCollapseHorario').addClass("border-success");

                                                                                                              $(this).addClass('bg-light');
                                                                                                               //$('#collapseHorario').collapse('show');
                                                                                                                /* Agregando valor al div del formulario*/ 
                                                                                                                //$('#horario_act').html($(this).text());
                                                                                                                  $('#horario_act').empty();
                                                                                                                  $("<span></span>", {
                                                                                                                      'class':'badge badge-success',
                                                                                                                      'text':$(this).text(),
                                                                                                                      }
                                                                                                                  ).appendTo('#horario_act')

                                                                                                                $("#enviarSolcitud").prop('disabled', false);
                                                                                                                horarios_ids.push($(this).children('.horario').text())
                                                                                                                console.log(horarios_ids)
                                                                                                                $("#input_horario").val(horarios_ids);

                                                                                                              }
                                                                                                            },
                                                                                                  }).appendTo('#contenedorHorario');
                                                                                              });


                                                                                          }
                                                                                          else {
                                                                                              //alert('Hubo un error Procesando Solicitud \n' + data.status);
                                                                                          }
                                                                                      });


                                                                    }
                                                                  },
                                                        }).appendTo('#contenedorPeriodos');
                                                    });


                                                }
                                                else {
                                                    alert('Hubo un error Procesando Solicitud \n' + data.status);
                                                }
                                            });



                                      }
                                    },
                          }).appendTo('#contenedorActividades');
                      });


                  }
                  else {
                      alert('Hubo un error Procesando Solicitud \n' + data.status);
                  }
              });

          });


  });


/*

///////////////////////////////Evento Cursos      ///////////////////////////////////////

            $(".periodo-items").click(function(){

              //alert("Tipowas clicked.");
               $('#collapseActividades').collapse('show');
                $('#collapseHorario').collapse('hide');
                // Agregando valor al div del formulario// 
                $('#periodo_act').html($(this).text());
                $('#curso_act').html('Selecciona una Actividad');
                $('#horario_act').html('');
                $("#enviarSolcitud").prop('disabled', true);
                $("#input_periodo").val($(this).children('.periodo').text());
                $("#input_actividad").val('');
                $("#input_horario").val('');


                ajax.jsonRpc("/inscripcion/get_actividades", 'call', {
                    input_tipo: $("input[name='input_tipo']").val(),
                    input_periodo: $("input[name='input_periodo']").val(),                    
                }).then(function (data) {
                    console.log(data);  
                    console.log("sstatus " + data.status);

                    if (data.status === 'done') {
                        //alert(data);
                        //alert('Solicitud Recibida');
                        //$('#contenedorPeriodos').

                        $.each(data.res.objeto , function (index, actividades) {
                            console.log("Periodo   : " + actividades);

                            $("<a></a>", {
                                'class': "list-group-item periodo-items",
                                'html': $("<h4></h4",{
                                      'class': 'periodo',
                                      'text' : actividades,
                                      }),

                                'href' : "#collapseCursos",
                                "on": {
                                        // attach `click` event to dynamically created element
                                        "click": function( event ) {

                                            $('#collapseHorario').collapse('show');
                                            $('#curso_act').html($(this).text())
                                            $('#horario_act').html('Selecciona un Horario');
                                            $("#enviarSolcitud").prop('disabled', true);
                                            $("#input_actividad").val($(this).children('.actividades').text());
                                            $("#input_horario").val('');

                                        }
                                      },
                            }).appendTo('#contenedorActividades');
                        });


                    }
                    else {
                        //alert('Hubo un error Procesando Solicitud \n' + data.status);
                    }
                });

            });

*/

/*
//////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////Evento de actividades///////////////////////////////////////


            $(".actividades-items").click(function(){

              //alert("Tipowas clicked.");
               //alert("Actividades was clicked.");
                $('#collapseHorario').collapse('show');
                $('#curso_act').html($(this).text())
                $('#horario_act').html('Selecciona un Horario');
                $("#enviarSolcitud").prop('disabled', true);
                $("#input_actividad").val($(this).children('.actividades').text());
                $("#input_horario").val('');


                ajax.jsonRpc("/inscripcion/get_horario", 'call', {
                    input_tipo: $("input[name='input_tipo']").val(),
                    input_periodo: $("input[name='input_periodo']").val(),
                    input_actividad: $("input[name='input_actividad']").val(),

                }).then(function (data) {
                    console.log(data);  
                    console.log("sstatus " + data.status);

                    if (data.status === 'done') {
                        alert(data);
                        alert('Solicitud Recibida');
                        //$('#contenedorPeriodos').

                        $.each(data.res.objeto , function (index, horario) {
                            console.log("actividades   : " + horario);

                            $("<a></a>", {
                                'class': "list-group-item actividades-items",
                                'html': $("<h4></h4",{
                                      'class': 'horario',
                                      'text' : horario,
                                      }),

                                "on": {
                                        // attach `click` event to dynamically created element
                                        "click": function( event ) {
                                          // Do something
                                         //alert("horario was clicked.");
                                         //$('#collapseHorario').collapse('show');
                                          $('#horario_act').html($(this).text());
                                          $("#enviarSolcitud").prop('disabled', false);
                                          $("#input_horario").val($(this).children('.horario').text());

                                        }
                                      },
                            }).appendTo('#contenedorHorario');
                        });


                    }
                    else {
                       // alert('Hubo un error Procesando Solicitud \n' + data.status);
                    }
                });

            });

/*
//////////////////////////////////////////////////////////////////////////////////////////////



/*
            var myMessages = ['info','warning','error','success'];

            function hideAllMessages() {
                     var messagesHeights = new Array(); // this array will store height for each

                     for (i=0; i<myMessages.length; i++) {
                      messagesHeights[i] = $('.' + myMessages[i]).outerHeight(); // fill array
                      $('.' + myMessages[i]).css('top', -messagesHeights[i]); //move element outside viewport     
                     }
            }
            
            hideAllMessages()

            function showMessage(type) {
                alert("llegamos a la funcion " + type)
                 $("#successMessage").show();

                $('.'+ type +'-trigger').click(function(){

                        hideAllMessages();                  
                        $('.'+type).animate({top:"0"}, 500);
                        $().hide

                });
            };
*/


            

            /*
            $(".periodo-items").click(function(){
               //alert("list-group-item was clicked.");
               $('#collapseActividades').collapse('show');
                $('#collapseHorario').collapse('hide');
                // Agregando valor al div del formulario// 
                $('#periodo_act').html($(this).text());
                $('#curso_act').html('Selecciona una Actividad');
                $('#horario_act').html('');
                $("#enviarSolcitud").prop('disabled', true);
                $("#input_periodo").val($(this).children('.periodo').text());
                $("#input_actividad").val('');
                $("#input_horario").val('');

            });
            */

/*
            $(".actividades-items").click(function(){
               //alert("Actividades was clicked.");
               $('#collapseHorario').collapse('show');
                // Agregando valor al div del formulario// 
                $('#curso_act').html($(this).text())
                $('#horario_act').html('Selecciona un Horario');
                $("#enviarSolcitud").prop('disabled', true);
                $("#input_actividad").val($(this).children('.actividades').text());
                $("#input_horario").val('');
              });

*/
  /*
            $(".horario-items").click(function(){
               //alert("horario was clicked.");
               //$('#collapseHorario').collapse('show');
                // Agregando valor al div del formulario// 
                $('#horario_act').html($(this).text());
                $("#enviarSolcitud").prop('disabled', false);
                $("#input_horario").val($(this).children('.horario').text());
            });

*/






    /*

    $(".tipo").click(function(){
      alert("list-group-item was clicked.");
       $('#collapsePeriodos').collapse('show');
       $('#collapseHorario').collapse('show');
       $('#collapseActividades').collapse('show');

    });

    $(".tipo").click(function(){
      alert("list-group-item was clicked.");
       $('#collapsePeriodos').collapse('show');
       $('#collapseHorario').collapse('show');
       $('#collapseActividades').collapse('show');

    });
    */
    /*
    var frm = $('#form_enviarSolicitud');

    frm.submit(function (e) {
        e.preventDefault();
          console.log($('#input_tipo').attr('value'));
          console.log($('#input_periodo').attr('value'));
          console.log($('#input_actividad').attr('value'));
          console.log($('#input_horario').attr('value'));
          console.log($('#input_horario').val());
          console.log(frm.serialize());

        $.ajax({
            dataType: "json",
            type:'POST',
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    });

*/

    /*   Notificaciones   */


      /* Fin   Notificaciones */
