# coding: utf-8
u"""
Controladores para las rutas de las Maestras (información de cursos y carreras)
Todos los controladores definidos acá deben iniciar con la ruta
`/control_estudios/maestras/`
"""
from odoo import http


class MaestrasController(http.Controller):

    @http.route('/control_estudios/maestras', auth='none')
    def inicio(self):
        maestras = http.request.env['maestra.tipo'].sudo().search([])
        data = {'tipos': maestras}
        return http.request.render('control_estudios.inicio_maestras', data)

    @http.route('/control_estudios/maestras', auth='none')
    def eleccion_maestra(self):
        maestras = http.request.env['maestra.tipo'].sudo().search([])
        periodos = http.request.env['maestra.periodo'].sudo().search([])
        horarios = http.request.env['clase.horario'].sudo().search([])


        data = {'tipos': maestras,
        'periodos': maestras,
        'horarios': maestras
        }


        self._logger.info(':::: Data   ::::  %s ', data )

        print ("------------------------"*20)
        return http.request.render('control_estudios.inicio_maestras', data)
    