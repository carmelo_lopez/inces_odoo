# coding: utf-8
import logging

from odoo import http
from odoo.http import request

import json
import qrcode
import base64



class MainController(http.Controller):
    _logger = logging.getLogger(__name__)

    @http.route('/inscripcion/inicio', auth='none')
    def inicio(self):
        return http.request.render('inscripcion.pagina_inicio', {})

    @http.route('/inscripcion/solicitudes_historial/<int:aspirante>/<int:solicitud>',auth='public', website=True)
    def solicitudes_historial(self,**args):
        self._logger.info('Mensaje recibido: args %s' %args)
        aspirante_id = http.request.env['aspirante'].sudo().search([('id','=',args['aspirante'])])
        solicitud = http.request.env['solicitud'].sudo().search([('id','=',args['solicitud'])])


        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        qr.add_data('AND THEEEN')
        qr.make(fit=True)

        img = qr.make_image(fill_color="black", back_color="white")
        #'qr':base64.b64encode(img.tobytes())
        return http.request.render('inscripcion.solicitudes_template', 
            {'solicitudes':aspirante_id.solicitud_ids,'comprobante_solicitud':solicitud})



    @http.route('/inscripcion/validar_solicitud', auth='public',)
    def validar_ingreso(self,idn=''):

        aspirante_id = http.request.env['aspirante'].sudo().search([('cedula','=',idn)])
        pais=http.request.env['res.country'].sudo().search([])
        estado=http.request.env['res.country.state'].sudo().search([])
        municipio=http.request.env['res.country.state.municipality'].sudo().search([])
        parroquia=http.request.env['res.country.state.municipality.parish'].sudo().search([])        
        if idn:

            diccionario_res={
                'paises':pais,
                'estados':estado,
                'municipios':municipio,
                'parroquias':parroquia,
                'idn':idn,
            }

            if aspirante_id:
                diccionario_res.update({
                    'aspirante' : aspirante_id 
                })
                return http.request.render('inscripcion.formulario_aspirante', diccionario_res)
            else:
                return http.request.render('inscripcion.formulario_aspirante', diccionario_res)

        return http.request.render('inscripcion.inicio_maestras', {})

    
    @http.route(['/inscripcion'], auth='public', website=True)
    def pagina_solicitudes(self, name='', idn='', email='',tipo_solicitante='',**args):

        periodos = http.request.env['formacion.periodo'].sudo().search([])
        horarios = http.request.env['clase.horario'].sudo().search([])


        tipos = http.request.env['modalidad'].sudo().search([])


        formacion = http.request.env['formacion'].sudo().search([])
        habilidades = set([x.habilidad_requerida for  x in formacion])


        habilidades_filtro = [x for  x in args if x in habilidades ]


        aspirante_id = http.request.env['aspirante'].sudo().search([('cedula','=',idn)])
        context = http.request.context.copy()
        context.update({
            'habilidades_filtro':habilidades_filtro
            })
        http.request.context = context

        pais=http.request.env['res.country'].sudo().search([])
        estado=http.request.env['res.country.state'].sudo().search([])
        municipio=http.request.env['res.country.state.municipality'].sudo().search([])
        parroquia=http.request.env['res.country.state.municipality.parish'].sudo().search([])        

        diccionario_res = {
            'tipo_solicitante': tipo_solicitante,
            'formacion':formacion,
            'habilidades':habilidades,
            'habilidades_filtro':habilidades_filtro,
            'tipos': tipos,
            'periodos': periodos,
            'horarios': horarios,
            'paises':pais,
            'estados':estado,
            'municipios':municipio,
            'parroquias':parroquia,
        }

        return http.request.render('inscripcion.inicio_maestras', diccionario_res)


    @http.route(['/inscripcion/formulario_aspirante'], methods=['post'], auth='public', type='json')
    def formulario_aspirante(self, name='', idn='', email='',tipo_solicitante='',**args):
        self._logger.info('Mensaje recibido: name = %s, idn = %s, email = %s', name, idn, email)
        self._logger.info('Mensaje recibido: args %s' %args)
        self._logger.info(':::: args   ::::  %s ' %args )
        self._logger.info(':::: -----------------------------------------------------------   :::: ')
        self._logger.info(':::: -----------------------------------------------------------   :::: ')
        self._logger.info(':::: -----------------------------------------------------------   :::: ')
        self._logger.info(':::: -----------------------------------------------------------   :::: ')

        tipos = http.request.env['modalidad'].sudo().search([])
        periodos = http.request.env['formacion.periodo'].sudo().search([])
        horarios = http.request.env['clase.horario'].sudo().search([])
        formacion = http.request.env['formacion'].sudo().search([])

        csrf_token = args['data'].pop('csrf_token', False)
        idn = args['data'].pop('idn', False)
        name = args['data'].pop('name', False)
        tipo_solicitante = args['data'].pop('tipo_solicitante', False)

        aspirante_id = http.request.env['aspirante'].sudo().search([('cedula','=',idn)])
        self._logger.info(':::: aspirante_id   ::::  %s ' %aspirante_id )

        dict_aspirante = {
            'cedula':idn,
        }
        dict_aspirante.update(args['data'])

        if not aspirante_id:
            ##sino existe la persona se crea con los datos suministrados

            aspirante_id = http.request.env['aspirante'].sudo().create(dict_aspirante)
            self._logger.info(':::: creado el aspirante    ::::  %s ' %aspirante_id )
        else:
            #aspirante_id.write(args)
            aspirante_id.write({x:args['data'][x] for x in args['data'] if args.get('data')})

        ################ -------------------------------------- ################
        ################ -------------------------------------- ################
        ################ -------------------------------------- ################

        input_solicitud = args['data_solicitud']

        formacion_id = http.request.env['formacion'].sudo().search([('name','=',input_solicitud['input_actividad'])])
        aspirante_id = http.request.env['aspirante'].sudo().search([('cedula','=',idn)])
        modalidad_id = http.request.env['modalidad'].sudo().search([('name','=',input_solicitud['input_tipo'])])
        self._logger.info(':::: solicitud  modalidad_id    modalidad_id ::::  %s ::: %s' %(modalidad_id,modalidad_id.id) )
        periodo_id = http.request.env['formacion.periodo'].sudo().search([
            ('modalidad_id','=',modalidad_id.id)])[0]

        #horario_ids = http.request.env['clase.horario'].sudo().search([('name','in',input_solicitud['input_horario'].split(","))])
        #horario_ids = http.request.env['clase.horario'].sudo().search([('name','in',input_solicitud['input_horario'].split(","))])



        if not aspirante_id:
            aspirante_id = 1
            return  {'status': 'No Existe aspirante con esa cedula, por favor verificar los datos',}

        self._logger.info(':::: solicitud Creada formacion_id    nro ::::  %s ' %formacion_id )
        self._logger.info(':::: solicitud Creada aspirante_id    nro ::::  %s ' %aspirante_id )
        self._logger.info(':::: solicitud Creada periodo_id    nro ::::  %s ' %periodo_id )
        self._logger.info(':::: solicitud tipo_solicitante     nro ::::  %s ' %tipo_solicitante )
        #self._logger.info(':::: solicitud horario_ids     nro ::::  %s ' % horario_ids )
        #self._logger.info(':::: solicitud tipo_solicitante     nro ::::  %s ' %input_solicitud['input_horario'].split(",") )


        dict_solicitud ={
            'formacion_id':formacion_id.id,
            'tipo':tipo_solicitante,
            'aspirante_id':aspirante_id.id,
            #'periodo_id':periodo_id.id,
            'periodo_id':periodo_id.id,
            #'horario_ids':[(6, 0, horario_ids.ids)],
            'comentario':args['data']['comentario']
            }

        solicitud = http.request.env['solicitud'].sudo().search([
            ('formacion_id','=',formacion_id.id),
            ('aspirante_id','=',aspirante_id.id),
            ('periodo_id','=',periodo_id.id),
            ])
        if not solicitud:
            crear_solicitud = http.request.env['solicitud'].sudo().create(dict_solicitud)

            diccionario_res = {
                'tipo_solicitante': tipo_solicitante,
                'formacion':formacion,
                'tipos': tipos,
                'periodos': periodos,
                #'horarios': horarios,
            }

            self._logger.info('::::  diccionario_res    ::::  %s ' %diccionario_res )
            return {'status': 'done','res':{'dict_aspirante':dict_aspirante,'aspirante':aspirante_id.id,'solicitud':crear_solicitud.id}}
        else:
            return {'status': 'Ya existe una Solicitud para %s en el periodo %s esperando ser Aprobada'%(formacion_id.name, periodo_id.name)}
        ################ -------------------------------------- ################
        ################ -------------------------------------- ################
        ################ -------------------------------------- ################



       
        #return http.request.render('inscripcion.inicio_maestras', diccionario_res)
        ##return http.request.redirect('/inscripcion')



    @http.route(['/inscripcion/enviar_solicitud'], methods=['post'], auth='public', type='json')
    def enviar_solicitud(self,idn='',name='',input_tipo='',input_periodo='',input_actividad='',input_horario='',tipo_solicitante='',**args):

        self._logger.info(':::: solicitud Creada idn    nro ::::  %s ' %idn )
        self._logger.info(':::: solicitud Creada name    nro ::::  %s ' %name )
        self._logger.info(':::: solicitud Creada input_tipo    nro ::::  %s ' %input_tipo )
        self._logger.info(':::: solicitud Creada input_periodo    nro ::::  %s ' %input_periodo )
        self._logger.info(':::: solicitud Creada input_actividad    nro ::::  %s ' %input_actividad )
        self._logger.info(':::: solicitud Creada input_horario    nro ::::  %s ' %input_horario )

        data = http.request.jsonrequest

        formacion_id = http.request.env['formacion'].sudo().search([('name','=',input_actividad)])
        aspirante_id = http.request.env['aspirante'].sudo().search([('cedula','=',idn)])
        modalidad_id = http.request.env['modalidad'].sudo().search([('name','=',input_tipo)])
        periodo_id = http.request.env['formacion.periodo'].sudo().search([('name','=',input_periodo),('modalidad_id','=',modalidad_id.id)])
        horario_ids = http.request.env['clase.horario'].sudo().search([('name','in',input_horario.split(","))])



        if not aspirante_id:
            aspirante_id = 1
            return  {'status': 'No Existe aspirante con esa cedula, por favor verificar los datos',}

        self._logger.info(':::: solicitud Creada formacion_id    nro ::::  %s ' %formacion_id )
        self._logger.info(':::: solicitud Creada aspirante_id    nro ::::  %s ' %aspirante_id )
        self._logger.info(':::: solicitud Creada periodo_id    nro ::::  %s ' %periodo_id )
        self._logger.info(':::: solicitud tipo_solicitante     nro ::::  %s ' %tipo_solicitante )
        self._logger.info(':::: solicitud horario_ids     nro ::::  %s ' % horario_ids )
        self._logger.info(':::: solicitud tipo_solicitante     nro ::::  %s ' %input_horario.split(",") )


        dict_solicitud ={
            'formacion_id':formacion_id.id,
            'tipo':tipo_solicitante,
            'aspirante_id':aspirante_id.id,
            'periodo_id':periodo_id.id,
            'horario_ids':[(6, 0, horario_ids.ids)]
            }


        crear_solicitud = http.request.env['solicitud'].sudo().create(dict_solicitud)
        self._logger.info(':::: solicitud Creada satisfactoriamente    nro ::::  %s ' %crear_solicitud.solicitud )

        return  {'status': 'done',}




    @http.route(['/inscripcion/get_actividades'], methods=['post'], auth='public', type='json')
    def get_actividades(self,input_tipo='',**args):
        self._logger.info(':::: http.request.context()   ::::  %s ' %http.request.context )

        self._logger.info(':::: solicitud Creada input_tipo    nro ::::  %s ' %input_tipo )
        self._logger.info(':::: solicitud Creada args    nro ::::  %s ' %args )
        self._logger.info(':::: solicitud Creada args    nro ::::  %s ' %args )
        self._logger.info(':::: habilidad_requerida   nro ::::  %s ' %type(args.get('habilidades_elegidas',[])) )
        self._logger.info(':::: modalidades_elegidas   nro ::::  %s ' % args.get('habilidades_elegidas',[]))
        self._logger.info(':::: modalidades_elegidas   nro ::::  %s ' % args.get('modalidades_elegidas'))
        self._logger.info(':::: modalidades_elegidas   nro ::::  %s ' % type(args.get('modalidades_elegidas',[])))



        modalidad_id = http.request.env['modalidad'].sudo().search([('name','=',input_tipo)])


        if not args.get('habilidades_elegidas',[]):
            formacion = http.request.env['formacion'].sudo().search(
                [('modalidad_id','in',[int(x) for x in args.get('modalidades_elegidas',[])]),
                ]
            )
        else:
            formacion = http.request.env['formacion'].sudo().search(
                [('modalidad_id','in',[int(x) for x in args.get('modalidades_elegidas',[])]),
                ('habilidad_requerida','in',args.get('habilidades_elegidas',[]))
                ]
            )


        diccionario_res = {
            'objeto': [ x.name for x in  formacion],
        }

        self._logger.info(':::: get_actividades diccionario_res    ::::  %s ' %diccionario_res )
        #return json.dumps({'status': 'done','res':diccionario_res})
        return {'status': 'done','res':diccionario_res}



    @http.route(['/inscripcion/get_periodos'], methods=['post'], auth='public', type='json')
    def get_periodos(self,input_tipo='',input_actividad='',**args):
        self._logger.info(':::: solicitud Creada input_tipo    nro ::::  %s ' %input_tipo )
        self._logger.info(':::: solicitud Creada input_actividad    nro ::::  %s ' %input_actividad )

        data = http.request.jsonrequest

        modalidad_id = http.request.env['modalidad'].sudo().search([('name','=',input_tipo)])

        periodos = http.request.env['formacion.periodo'].sudo().search([('modalidad_id','=',modalidad_id.id)])

        diccionario_res = {
            'objeto': [ x.name for x in  periodos],
        }

        self._logger.info(':::: get_periodos     ::::  %s ' %diccionario_res )
        #return json.dumps({'status': 'done','res':diccionario_res})
        return {'status': 'done','res':diccionario_res}


    @http.route(['/inscripcion/get_horario'], methods=['post'], auth='public', type='json')
    def get_horario(self,input_tipo='',input_periodo='',input_actividad='',**args):

        self._logger.info(':::: solicitud Creada input_tipo    nro ::::  %s ' %input_tipo )
        self._logger.info(':::: solicitud Creada input_periodo    nro ::::  %s ' %input_periodo )
        self._logger.info(':::: solicitud Creada input_actividad    nro ::::  %s ' %input_actividad )
        data = http.request.jsonrequest

        horarios = http.request.env['clase.horario'].sudo().search([])

        diccionario_res = {
            'objeto': [ x.name for x in  horarios],
        }

        self._logger.info(':::: get_horario diccionario_res    ::::  %s ' %diccionario_res )
        #return json.dumps({'status': 'done','res':diccionario_res})
        return {'status': 'done','res':diccionario_res}


    @http.route(['/inscripcion/get_cedula'], auth='public', type='json')
    def get_cedula(self,idn='',**args):
        idn = args['cedula']
        aspirante_id = http.request.env['aspirante'].sudo().search([('cedula','=',idn)])
        aspirante_read = http.request.env['aspirante'].sudo().read([aspirante_id.id],[])

        self._logger.info(':::: solicitud     args  ::::  %s ' %args )

        self._logger.info(':::: Consulta Cedula   nro ::::  %s ' %aspirante_read )

        if not aspirante_id:
            return  {'status': 'no_existe',}
        diccionario_res ={
            'primer_nombre':aspirante_id.primer_nombre,
            'segundo_nombre':aspirante_id.segundo_nombre,
            'primer_apellido':aspirante_id.primer_apellido,
            'segundo_apellido':aspirante_id.segundo_apellido,
            'zip_code':aspirante_id.zip_code,
            'street':aspirante_id.street,
            'country_id':aspirante_id.country_id.id,
            'state_id':aspirante_id.state_id.id,
            'municipality_id':aspirante_id.municipality_id.id,
            'parish_id':aspirante_id.parish_id.id,
            'street':aspirante_id.street,
            'fecha_nacimiento':aspirante_id.fecha_nacimiento,
            'telefono_aspirante':aspirante_id.telefono_aspirante,
            'correo_aspirante':aspirante_id.correo_aspirante,
            'genero':aspirante_id.genero,
        }
        #return http.request.render('inscripcion.formulario_aspirante', {})

        return {'status': 'done','res':diccionario_res}


    @http.route(['/inscripcion/get_formacion_informacion'], auth='public', type='json')
    def get_formacion_informacion(self,formacion_id='',**args):
        formacion_id = args['formacion']
        formacion = http.request.env['formacion'].sudo().search([('name','=',formacion_id)])

        self._logger.info(':::: get_formacion_informacion     args  ::::  %s ' %args )
        ##self._logger.info(':::: formacion     formacion  ::::  %s ' %formacion.foto )
        self._logger.info(':::: formacion.foto       ::::  %s ' %type(formacion.foto) )


        if not formacion:
            return  {'status': 'no_existe',}
        diccionario_res ={
            'formacion':formacion.name,
            'formacion_subtitulo':formacion.subtitulo,
            'habilidad_requerida':formacion.habilidad_requerida,
            'descripcion':formacion.descripcion,
            'foto':formacion.foto,
            'duracion':formacion.duracion,
            'modalidad':formacion.modalidad_id.name,


        }
        #return http.request.render('inscripcion.formulario_aspirante', {})
        ###self._logger.info(':::: diccionario_res       ::::  %s ' %(diccionario_res) )

        return {'status': 'done','res':diccionario_res}
