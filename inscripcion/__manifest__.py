# coding: utf-8
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Inscripciones INCES',
    'version': '10.0.1.0.0',
    'author': 'FaCyT',
    'maintainer': 'FaCyT',
    'website': 'https://gitlab.com/carmelo_lopez/inces_odoo',
    'license': 'AGPL-3',
    'category': 'Extra Tools',
    'summary': 'Gestión de Inscripciones del INCES',
    'depends': ['control_estudios','website'],
    'data': [
        # Seguridad
        'security/ir.model.access.csv',
        # Vistas comunes
        'views/main.xml',
        'views/aspirante.xml',
        'views/solicitud.xml',
        'views/documento.xml',

        #extension del modelo formacion
        'views/extend_formacion.xml', ### 

        # Páginas de los controllers
        'static/src/xml/inicio.xml',
        'static/src/xml/validar_ingreso.xml',
        'static/src/xml/formulario_aspirante.xml',
        'static/src/xml/carousel_template.xml',
        'static/src/xml/solicitudes_template.xml',
        'static/src/xml/habilidades_requeridas.xml',
        'static/src/xml/maestras/inicio_maestras.xml',
        'static/src/xml/maestras/inicio_maestras_recomendaciones.xml',

        ## Reportes 
        'reports/reporte_analitico_de_solicitudes.xml',
        'reports/reporte_analitico_de_aspirantes.xml',
    ],
    'installable': True,
    'application': True,
}
