# coding: utf-8
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class Caracterizacion(models.Model):
    _name = 'caracterizacion'
    _description = 'Caracterizacion de cada Integrante INCES'
    _rec_name = 'cedula'

    nivel_idioma = [('basico', 'Basico'),
        ('medio', 'Medio'),
        ('avanzado', 'Avanzado'),
    ]


    estado_civil = [('soltero', 'Solter'),
        ('casado', 'Casado'),
        ('divorciado', 'Divorciado'),
        ('viudo', 'Viudo'),
        ('concubino', 'Concubino'),
    ]

    nivel_estudio = [
        ('culmino','Culminado'),
        ('incompleto','Imcompleto'),
        ('pendiente','Pendiente'),
    ]   


    condicion_tenencia_vivienda = [
        ('propia','Propia'),
        ('pagando','Pagando'),
        ('alquilada','Alquilada'),
        ('prestada','Prestada'),
        ('compartida','Compartida'),
        ('invadida','Invadida'),
        ('en_construccion','En construccion'),
    ]

    tipo_vivienda = [
        ('edificio','edificio'),
        ('quinta','quinta'),
        ('casa','casa'),
        ('casa_vecinal','casa_vecinal'),
        ('pension','pension'),
        ('anexo','anexo'),
    ]



    tipo_vialidad = [
        ('veredas','veredas'),
        ('tierra','tierra'),
        ('asfalto','asfalto'),
        ('fluvial','fluvial'),
    ]





    nacionalidad = fields.Selection([('V', 'Venezolano'), ('E', 'Extranjero')], required=True, default='V', copy=False)
    cedula = fields.Char('Cédula', required=True, help='Sin puntos, solo los dígitos, ejemplo 12345678', copy=False)
    primer_nombre = fields.Char(required=True, copy=False)
    segundo_nombre = fields.Char(copy=False)
    primer_apellido = fields.Char(required=True, copy=False)
    segundo_apellido = fields.Char(copy=False)
    nombre_completo = fields.Char(compute='_compute_nombre_completo')
    active = fields.Boolean(default=True, copy=False)
    genero = fields.Selection([('M', 'Masculino'), ('F', 'Femenino')], 'Género')
    fecha_nacimiento = fields.Date(copy=False)
    edad = fields.Integer(compute='_compute_edad')
    foto = fields.Binary(copy=False, attachment=True)
    user_id = fields.Many2one('res.users', 'Usuario')
    email = fields.Char('Correo', related='user_id.email')
    phone = fields.Char('Teléfono', related='user_id.phone')

    ###8
    pertenece_pueblo_indigena = fields.Boolean(default=False, copy=False)
    domina_idioma_pueblo_indigena = fields.Boolean(default=False, copy=False)
    pueblo_indigena = fields.Char('Pueblo Indigena')
    nivel_escritura_ind = fields.Selection( nivel_idioma, 'Dominio escrito del lenguaje',
                            required=True, default='basico',
                            help='Indica el nivel del Lenguaje')
    nivel_verbal_ind = fields.Selection( nivel_idioma, 'Dominio verbal del lenguaje',
                            required=True, default='basico',
                            help='Indica el nivel del Lenguaje')


    ### 9

    tiene_discapacidad = fields.Boolean(default=False, copy=False)
    certificado_de_discapacidad = fields.Boolean(default=False, copy=False)
    pension_de_discapacidad = fields.Boolean(default=False, copy=False)
    tipo_discapacidad = fields.Char('Tipo de discapacidades')
    codigo_conapdis = fields.Char('Código de CONAPDIS')

    ### 10 

    comunidad_inmigrante = fields.Boolean(default=False, copy=False)
    pais_origen = fields.Char('Pais Origen')
    pais_origen_religion = fields.Char('Religion')

    ### 11
    estado_civil = fields.Selection( estado_civil, 'Estado Civil',
                            required=True, default='soltero',
                            help='Estado Civil de la persona')
    ### 12
    #####################Datos Familiares########################
    tiene_hijos = fields.Boolean(default=False, copy=False)
    cantidad_hijos = fields.Integer('Cantidad de Hijos')
    hijos_0_12 = fields.Boolean(default=False, copy=False)
    hijos_13_18 = fields.Boolean(default=False, copy=False)
    hijos_19_mas = fields.Boolean(default=False, copy=False)
    tiene_hijos_con_discapacidad = fields.Boolean(default=False, copy=False)
    tipo_discapacidad_hijo = fields.Char('Discapacidades del o los Hijos')
    certificado_de_discapacidad_hijo = fields.Boolean(default=False, copy=False)
    codigo_conapdis_hijo = fields.Char('Código de CONAPDIS')

    ##13 contacto emergencia
    nombre_contacto_emergencia1 = fields.Char('Nombre del contacto para emergencias')
    parentezco_contacto_emergencia1 = fields.Char('Parentezco')
    telefono_contacto_emergencia1 = fields.Char('Telefono')

    nombre_contacto_emergencia2 = fields.Char('Nombre del contacto para emergencias')
    parentezco_contacto_emergencia2 = fields.Char('Parentezco')
    telefono_contacto_emergencia2 = fields.Char('Telefono')

    ##14 Familiares, solo 2

    nombre_contacto_familia1 = fields.Char('Nombre del contacto para familias')
    parentezco_contacto_familia1 = fields.Char('Parentezco')
    telefono_contacto_familia1 = fields.Char('Telefono')

    nombre_contacto_familia2 = fields.Char('Nombre del contacto para familias')
    parentezco_contacto_familia2 = fields.Char('Parentezco')
    telefono_contacto_familia2 = fields.Char('Telefono')


    #############Datos SOCIOCULTURALES########################

    practica_deporte =  fields.Boolean(default=False, copy=False)
    deportes = fields.Char('Deportes')
    tiempo_practicado = fields.Char('Tiempo Practicado')
    motivo_deporte = fields.Selection([
        ('recreeativo', 'recreeativo'), 
        ('competitivo', 'competitivo'),
        ('salud', 'salud')
        ], 
        required=True, default='recreeativo', copy=False)

    practica_cultural =  fields.Boolean(default=False, copy=False)
    actividades_culturales = fields.Char('Cultural')
    uso_del_tiempo_libre = fields.Char('Actividades adicionales en tiempo libres')




    #############Datos SocioPoliticos########################

    pertenece_organizacion_politica =  fields.Boolean(default=False, copy=False)
    nombre_organizacion_politica = fields.Char('Organizacion Politica')

    pertenece_movimiento_social =  fields.Boolean(default=False, copy=False)
    nombre_movimiento_social = fields.Char('Movimiento Social')

    pertenece_consejo_comunal =  fields.Boolean(default=False, copy=False)
    nombre_consejo_comunal = fields.Char('Consejo Comunal')

    participa_en_consejo_comunal = fields.Boolean(default=False, copy=False)
    participa_en_vocero = fields.Boolean(default=False, copy=False)
    participa_en_colaborador = fields.Boolean(default=False, copy=False)

    pertenece_comite_de_trabajo_cc = fields.Boolean(default=False, copy=False)
    actividades_comite_de_trabajo_cc = fields.Char('Actividades que desempeña')


    pertenece_mision_social = fields.Boolean(default=False, copy=False)
    actividades_mision_social = fields.Char('Actividades que desempeña')

    pertenece_mision_social = fields.Boolean(default=False, copy=False)
    actividades_mision_social = fields.Char('Actividades que desempeña')


    beneficiario_mision_social = fields.Boolean(default=False, copy=False)
    beneficiario_mision_social = fields.Char('Explique')


    pertenece_organizacion_socioproductiva = fields.Boolean(default=False, copy=False)
    actividades_organizacion_socioproductiva = fields.Char('Actividades que desempeña')

    crear_organizacion_socioproductiva = fields.Boolean(default=False, copy=False)

    inscripcion_cne = fields.Boolean(default=False, copy=False)

    ############Formacion Empirica y Academica#######################
    educacion_basica = fields.Selection( nivel_estudio, 'Educacion Basica',
        required=True, default='pendiente',
        help='Estado Civil de la persona')

    educacion_media = fields.Selection( nivel_estudio, 'Educacion Media',
                            required=True, default='pendiente',
                            help='Estado Civil de la persona')

    tsu = fields.Selection( nivel_estudio, 'tsu',
                            required=True, default='pendiente',
                            help='Estado Civil de la persona')

    universitario = fields.Selection( nivel_estudio, 'universitario',
                            required=True, default='pendiente',
                            help='Estado Civil de la persona')

    postgrado = fields.Selection( nivel_estudio, 'postgrado',
                            required=True, default='pendiente',
                            help='Estado Civil de la persona')

    alfabetizado = fields.Boolean(default=False, copy=False)

    ultimo_estudio_realizado = fields.Char('Ultimo Estudio Realizado')
    titulo_obtenido = fields.Char('Título Obtenido')

    cursos_talleres_otros = fields.Char('Otros Estudios, Cursos, Talleres Realizado')


    ##### 31. Experiencia Docente académica: 
    tiene_experiencia_laboral =  fields.Boolean(default=False, copy=False)

    anios_labores = fields.Integer('Años de Experiencia Laboral')
    area_conocimiento = fields.Char('Area laboral de Experiencia')

    nivel_educativo_estudio = fields.Selection([
    	('basica', 'Basico'), 
    	('diversificada', 'Diversificado'),
    	('tsu', 'TSU'),
    	('universitario', 'Universitario'),
    	('postgrado', 'Postgrado'),
    	('otro', 'Otro'),
    	],  'Nivel de Educacion de Desempeño',
    	required=True, default='basica', copy=False,help='Nivel de educacion')

    otros_trabajos = fields.Char('Otros Trabajos de desempeño')

    ocupacion = fields.Char('Ocupacion')
    anios_ocupacion = fields.Integer('Años de Experiencia Ocupacion')
    entidad_trabajo = fields.Char('Entidad de Trabajo')
    cargo_desempeno = fields.Char('Cargo Desempeño')
    fecha_ingreso = fields.Char('Fecha de Ingreso')
    anios_servicio = fields.Integer('Años de  Servicio')
    direccion_entidad = fields.Char('Direccion de Entidad')
    tlf_entidad = fields.Char('Numero de Telefono')

    ##32
    condicion_laboral = fields.Selection([
    	('ocupado', 'Ocupado'), 
    	('dependiente', 'Dependiente'),
    	('independiente', 'Independiente'),
    	('desocupado', 'Desocupado'),
    	],  'Nivel de Educacion de Desempeño',
    	required=True, default='basica', copy=False,help='Condicion Laboral')


    ##################### Datos de Salud de Sujeto Social Protagonico

    padece_enferdad = fields.Boolean(default=False, copy=False)
    especifique_enfermedad = fields.Char('Especifique Enfermedad')

    convulsiones = fields.Boolean(default=False, copy=False)
    dolores_cabeza = fields.Boolean(default=False, copy=False)
    insomnio = fields.Boolean(default=False, copy=False)
    mareos_frecuentes = fields.Boolean(default=False, copy=False)
    convulsiones = fields.Boolean(default=False, copy=False)
    asma = fields.Boolean(default=False, copy=False)
    vista = fields.Boolean(default=False, copy=False)
    caries = fields.Boolean(default=False, copy=False)
    otros = fields.Boolean(default=False, copy=False)
    especifique_sintomas = fields.Char('Especifique Sintomas')


    esta_en_control_medico = fields.Boolean(default=False, copy=False)
    especifique_control_medico = fields.Char('Control Medico')


    consume_bebidas_alcoholicas = fields.Boolean(default=False, copy=False)
    fumador_frecuente = fields.Boolean(default=False, copy=False)


	####datos vivienda

    sel_condicion_tenencia_vivienda = fields.Selection( condicion_tenencia_vivienda, 'Condicion de vivienda actual',
                            required=True, default='propia',)

    sel_tipo_vivienda = fields.Selection( tipo_vivienda, 'Tipo de vivienda',
                            required=True, default='edificio',)

    sel_tipo_vialidad = fields.Selection( tipo_vialidad, 'Tipo de Vialidad',
                            required=True, default='veredas',)

    servicios_basicos = fields.Char('Servicios basicos con los que cuenta')


