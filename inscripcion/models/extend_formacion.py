# coding: utf-8
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
from odoo.modules import get_module_resource
import base64


class Formacion(models.Model):
	_inherit = 'formacion'
	habilidad_requerida =   fields.Char('Habilidad Requerida', required=False)
	subtitulo =   fields.Char('Subtitulo Descriptivo de la Formación', required=False)
	descripcion =   fields.Text('Descripcion de la Formación', required=False)
	foto = fields.Binary(copy=False, attachment=True)




	@api.model
	def create(self, vals):
	#img_path = False
		if not vals.get('foto'):
			img_path = get_module_resource('control_estudios', 'static/description', 'icon.png')
			if img_path:
				with open(img_path, 'rb') as img_file:
					vals['foto'] = base64.b64encode(img_file.read())

		return super(Formacion, self).create(vals)