# coding: utf-8
from odoo import fields, models


class Aspirante(models.Model):
    _name = 'aspirante'
    _description = 'Aspirante a Participante INCES'
    _inherit = 'persona'

    nuevo_ingreso = fields.Boolean(default=True)
    correo_aspirante = fields.Char('Correo Electronico',)
    telefono_aspirante = fields.Char('Nro Telefonico de Contacto')

    solicitud_ids = fields.One2many('solicitud', 'aspirante_id',
                                    string='Solicitudes', copy=False)
    documento_ids = fields.One2many('documento', 'aspirante_id',
                                    string='Documentos', copy=False)

