# coding: utf-8
from odoo import api, fields, models
import logging

_logger = logging.getLogger(__name__)


class Solicitud(models.Model):
    _name = 'solicitud'
    _description = 'solicitudes de Participante INCES'
    _rec_name = 'solicitud'
    _order = 'solicitud'

    formacion_id = fields.Many2one('formacion', 'Formacion', required=True)
    tipo = fields.Selection([('participante', 'Participante'),
                             ('facilitador', 'Facilitador'),
                             ('supervisor', 'Supervisor'),
                             ('otro', 'Otro')], 'Tipo de Participante',
                            required=True, default='participante',
                            help='Indica el tipo de de solicitud')
    aspirante_id = fields.Many2one('aspirante', 'Aspirante', required=True)
    periodo_id = fields.Many2one('formacion.periodo', 'Periodo', required=True)
    horario_ids = fields.Many2many('clase.horario', string='Horarios')
    modalidad_id = fields.Char('Modalidad', required=False, compute='_obtener_modalidad', store=True)
    comentario = fields.Char(required=False, copy=False)
    state = fields.Selection([
        ('no_aprobada', 'Solcitud No Aprobada'),
        ('aprobada', 'Solcitud Aprobada'),
    ], default='no_aprobada', copy=False, required=True)

    solicitud = fields.Char(compute='_compute_codigo_solicitud')
    aspirante_id_nombre = fields.Char(related='aspirante_id.nombre_completo',
                                      readonly=True)

    @api.multi
    @api.depends('aspirante_id', 'formacion_id', 'tipo', 'periodo_id')
    def _compute_codigo_solicitud(self):
        for record in self:
            record.solicitud = ''.join(name for name in (record.aspirante_id.cedula, str(record.aspirante_id.id),
                                                         str(record.formacion_id.id), str(record.periodo_id.id)) if name)

    @api.multi
    @api.depends('formacion_id',)
    def _obtener_modalidad(self):
        for record in self:
            record.modalidad_id = record.formacion_id.modalidad_id.name

    @api.multi
    def boton_aprobar_solicitud(self):
        for solicitud in self:
            solicitud.state = 'aprobada'
            if solicitud.tipo == 'participante':
                alumno = self.env['alumno']
                aspirante = self.env['aspirante']
                alumno_search = alumno.search([('cedula', '=', solicitud.aspirante_id.cedula)])
                aspirante_search = aspirante.search([('cedula', '=', solicitud.aspirante_id.cedula)])[0]

                dic_alumno = solicitud.aspirante_id

                res = solicitud.aspirante_id.read([
                    'primer_nombre', 'segundo_nombre', 'primer_apellido',
                    'segundo_apellido', 'cedula', 'country_id',
                    'state_id', 'genero', 'fecha_nacimiento',
                    'municipality_id', 'parish_id', 'correo_aspirante', 'telefono_aspirante'
                ])[0]
                res.update({
                    'country_id': res['country_id'][0],
                    'state_id': res['state_id'][0],
                    'municipality_id': res['municipality_id'][0],
                    'parish_id': res['parish_id'][0],
                })

                if alumno_search:
                    alumno_search.write(res)
                else:
                    print(res)
                    alumno.create(res)

                formacion = self.env['formacion']
                formacion = formacion.search([('name', '=', solicitud.formacion_id.name)])
            elif solicitud.tipo == 'facilitador':
                facilitador = self.env['facilitador']
                aspirante = self.env['aspirante']
                facilitador_search = facilitador.search([('cedula', '=', solicitud.aspirante_id.cedula)])
                aspirante_search = aspirante.search([('cedula', '=', solicitud.aspirante_id.cedula)])[0]

                dic_facilitador = solicitud.aspirante_id

                res = solicitud.aspirante_id.read([
                    'primer_nombre', 'segundo_nombre', 'primer_apellido',
                    'segundo_apellido', 'cedula', 'country_id',
                    'state_id', 'genero', 'fecha_nacimiento',
                    'municipality_id', 'parish_id'
                ])[0]
                res.update({
                    'country_id': res['country_id'][0],
                    'state_id': res['state_id'][0],
                    'municipality_id': res['municipality_id'][0],
                    'parish_id': res['parish_id'][0],
                })

                if facilitador_search:
                    facilitador_search.write(res)
                else:
                    print(res)
                    facilitador.create(res)

            print("OK.!!")


class Horario(models.Model):
    _inherit = 'clase.horario'

    @api.multi
    def name_get(self):
        result = []
        if self._context.get('periodo_id') and self._context.get('formacion_id'):
            periodo_id = self._context.get('periodo_id')
            formacion_id = self._context.get('formacion_id')
            solicitudes = self.env['solicitud'].search([('periodo_id', '=', periodo_id), ('formacion_id', '=', formacion_id)])

            cantidad_interesados = {}
            if solicitudes:
                for solicitud in solicitudes:
                    for horario in solicitud.horario_ids.ids:
                        if horario in cantidad_interesados:
                            cantidad_interesados[horario] = cantidad_interesados[horario] + 1
                        else:
                            cantidad_interesados[horario] = 1
                horario_ids = [x for x in sorted(cantidad_interesados.items(), key=lambda x:x[1])]

                # for record in self:
                #    result.append((record.id, str(record.name)))
                valores = {x.id: x.name for x in self.search([])}
                for record in horario_ids:
                    result.append((record[0], "%s ( %s )" % (valores[record[0]], str(record[1]))))
                    valores.pop(record[0])

                result += [(v, "%s ( 0 )" % (valores[v])) for v in valores]

            else:
                for record in self:
                    result.append((record.id, str(record.name)))

        else:
            for record in self:
                result.append((record.id, str(record.name)))
        return result


class Clase(models.Model):
    _inherit = 'clase'

    horario_ids = fields.Many2many('clase.horario', string='Horarios', compute='_compute_aspirantes_interesados')

    @api.multi
    @api.depends('periodo_id')
    def _compute_aspirantes_interesados(self):
        for clase in self:
            if clase.periodo_id:
                solicitudes = self.env['solicitud'].search([('periodo_id', '=', clase.periodo_id.id), ('formacion_id', '=', clase.formacion_id.id)])
                cantidad_interesados = {}

                for solicitud in solicitudes:
                    for horario in solicitud.horario_ids.ids:
                        if horario in cantidad_interesados:
                            cantidad_interesados[horario] = cantidad_interesados[horario] + 1
                        else:
                            cantidad_interesados[horario] = 1
                self.horario_ids = [x[0] for x in (sorted(cantidad_interesados.items(), key=lambda x:x[1]))][::-1]
