# coding: utf-8
from odoo import api, fields, models


class Documento(models.Model):
    _name = 'documento'
    _description = 'Documentos de los Participantes / Aspirantes'
    _rec_name = 'codigo'
    _order = 'codigo'

    codigo = fields.Char('Codigo del Documento',
                         required=True,
                         help='Codigo del documento asosciado a un aspirane',
                         compute='_compute_codigo')
    tipo_documento = fields.Char(required=True, copy=False)
    nombre_documento = fields.Char(copy=False, help='Nombre descriptivo del Documento')
    descripcion_documento = fields.Text(copy=False, help='Descripción del Documento')
    documento = fields.Binary('Documento para subir')
    active = fields.Boolean(default=True, copy=False)
    aspirante_id = fields.Many2one('aspirante', 'Aspirante', required=True)

    @api.multi
    def name_get(self):
        return [(record.id, '[%s] %s' % (record.codigo, record.nombre_documento)) for record in self]

    @api.multi
    @api.depends('aspirante_id', 'tipo_documento', 'nombre_documento')
    @api.onchange('aspirante_id', 'tipo_documento', 'nombre_documento')
    def _compute_codigo(self):
        for record in self:
            cedula = record.aspirante_id.cedula if record.aspirante_id else ''
            tipo_d = record.tipo_documento if record.tipo_documento else ''
            nombre_d = record.nombre_documento if record.nombre_documento else ''
            record.codigo = cedula + tipo_d + nombre_d
