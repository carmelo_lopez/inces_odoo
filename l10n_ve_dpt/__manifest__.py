# coding: utf-8
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Localización Venezolana: Municipios y Parroquias',
    'version': '10.0.2.0.1',
    'author': 'BachacoVE',
    'maintainer': 'BachacoVE',
    'website': 'http://www.bachaco.org.ve',
    'license': 'AGPL-3',
    'category': 'Localization',
    'summary': 'Estados y municipios de Venezuela.',
    'depends': ['base'],
    'data': [
        'data/res.country.state.xml',
        'data/res.country.state.municipality.xml',
        'data/res.country.state.municipality.parish.xml',
        'views/l10n_ve_dpt_view.xml',
        'views/res_partner.xml',
        'security/ir.model.access.csv'
    ],
}
