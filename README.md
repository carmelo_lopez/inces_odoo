[![pipeline status](https://gitlab.com/carmelo_lopez/inces_odoo/badges/master/pipeline.svg)](https://gitlab.com/carmelo_lopez/inces_odoo/commits/master)

# INCES ODOO

Proyecto para desarrollo de herramientas en Odoo, que permitan la gestión de
Control de Estudios en la institución INCES

## Autores

- Jissel Rodríguez, [jisselrh](https://gitlab.com/jisselrh)
- Hernan Neuman, [hneuman](https://gitlab.com/hneuman)
- Javier Carrasco, [javier-carrasco](https://gitlab.com/javier-carrasco)
- Carmelo López, [carmelo-lopez](https://gitlab.com/carmelo_lopez)

## Configuración

### Activar unaccent en postgres (para que las búsquedas no distingan el acento)

Se debe ejecutar el siguiente comando, donde cambiaremos `<db_name>` por el
nombre de la base de datos que usemos.
```
sudo -u postgres psql <db_name> -c "CREATE EXTENSION unaccent;"
```

Además, se debe activar la opción en el archivo de configuración de Odoo, sólo
agregamos la línea al final del documento:
```
unaccent = True
```
