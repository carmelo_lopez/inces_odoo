# coding: utf-8
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Asistencias INCES',
    'version': '10.0.1.0.0',
    'author': 'FaCyT',
    'maintainer': 'FaCyT',
    'website': 'https://gitlab.com/carmelo_lopez/inces_odoo',
    'license': 'AGPL-3',
    'category': 'Extra Tools',
    'summary': 'Registro de asistencias de facilitadores y alumnos INCES',
    'depends': ['control_estudios'],
    'data': [
        # Seguridad
        'security/groups.xml',
        'security/asistencia_security.xml',
        # Reglas de acceso
        'security/ir.model.access.csv',
        # Vistas comunes
        'wizards/cargar_asistencia_view.xml',
        'views/facilitador.xml',
        'views/asistencia_view.xml',
        'views/asistencia_linea_view.xml',
        'views/menues.xml',
        # Reportes
        'reports/hoja_asistencias.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'demo': [
    ],
}
