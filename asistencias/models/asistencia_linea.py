from odoo import models, fields, api


class AsistenciaLinea(models.Model):
    _name = "asistencia.linea"
    _inherit = ["mail.thread"]
    _rec_name = "alumno_id"
    _description = "Linea de Asistencia"
    _order = "fecha_clase desc"

    asistencia_id = fields.Many2one(
        'asistencia', 'Hoja de Asistencia', required=True,
        track_visibility="onchange", ondelete="cascade")
    state = fields.Selection(
        related='asistencia_id.state', store=True, readonly=True)
    alumno_id = fields.Many2one(
        'alumno', 'Alumno', required=True, track_visibility="onchange")
    asistente = fields.Boolean(
        '¿Asistió?', default=True, track_visibility="onchange")
    facilitador_id = fields.Many2one(
        'facilitador', 'Facilitador',
        related='asistencia_id.facilitador_id', readonly=True)
    formacion_id = fields.Many2one(
        'formacion', 'Formación',
        related='asistencia_id.formacion_id', readonly=True)
    unidad_id = fields.Many2one(
        'unidad.curricular', 'Unidad Curricular',
        related='asistencia_id.unidad_id', readonly=True)
    clase_id = fields.Many2one(
        'clase', 'Clase',
        related='asistencia_id.clase_id', readonly=True)
    nota = fields.Char('Nota', size=256, track_visibility="onchange")
    fecha_clase = fields.Date(
        'Fecha Clase', related='asistencia_id.fecha_clase', store=True,
        readonly=True, track_visibility="onchange")

    _sql_constraints = [
        ('unique_alumno',
         'unique(alumno_id,asistencia_id,fecha_clase)',
         'No se pueden repetir alumnos en una hoja de asistencias.'),
    ]

    @api.onchange('asistencia_id')
    def onchange_asistencia_id(self):
        return {
            'value': {'alumno_id': False},
        }
