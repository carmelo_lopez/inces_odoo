from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class FacilitadorTipo(models.Model):
    _name = 'facilitador.tipo'
    _description = 'Tipos de facilitador'

    name = fields.Char(string='Nombre', required=True, copy=False)
    horas_laborables = fields.Selection(
        string='Máximo de horas diarias laborables', default=4, required=True,
        copy=False, selection=[
            (4, '4 horas'),
            (5, '5 horas'),
            (6, '6 horas'),
            (6, '7 horas'),
            (8, '8 horas')
        ]
    )
    horas_remunerables = fields.Selection(
        string='Horas diarias remunerables', default=4, required=True,
        copy=False, selection=[
            (0, '0 horas'),
            (1, '1 hora'),
            (2, '2 horas'),
            (3, '3 horas'),
            (4, '4 horas'),
            (5, '5 horas'),
            (6, '6 horas'),
            (6, '7 horas'),
            (8, '8 horas')
        ]
    )
    facilitador_ids = fields.One2many(
        'facilitador', 'tipo_id', string='Facilitadores', copy=False)
    qty_facilitadores = fields.Integer(
        string="Cantidad de Facilitadores",
        compute='_compute_qty_facilitadores')

    _sql_constraints = [
        ('unique_name', 'unique(name)', _(
            'Ya existe este tipo de facilitador.'))
    ]

    @api.depends('facilitador_ids')
    def _compute_qty_facilitadores(self):
        for record in self:
            record.qty_facilitadores = len(record.facilitador_ids)

    @api.constrains('horas_remunerables')
    def check_horas_remunerables(self):
        for record in self:
            if record.horas_remunerables > record.horas_laborables:
                raise ValidationError(_(
                    'Las horas remunerables deben ser menor o igual a las  \
                     horas laborables'))
            elif record.horas_remunerables < 0:
                raise ValidationError(_(
                    'Las horas remunerables deben ser mayor o igual a 0'))

    @api.onchange('horas_laborables')
    def _onchange_asignatura(self):
        return {
            'value': {'horas_remunerables': self.horas_laborables},
        }


class Facilitador(models.Model):
    _name = 'facilitador'
    _inherit = 'facilitador'

    tipo_id = fields.Many2one(
        'facilitador.tipo', 'Tipo de facilitador')
    fecha_ingreso = fields.Date('Fecha de Ingreso')

