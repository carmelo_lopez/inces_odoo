from odoo import _, models, fields, api
from datetime import datetime
from odoo.exceptions import ValidationError


DATE_FORMAT = '%d/%m/%Y'


class Asistencia(models.Model):
    _name = "asistencia"
    _inherit = ["mail.thread"]
    _description = "Hoja de asistencias a clases"
    _order = "fecha_clase desc"

    name = fields.Char('Name', required=True, compute='_compute_name')
    state = fields.Selection([
        ('borrador', 'Borrador'),
        ('aprobada', 'Aprobada'),
    ], string="Estado", default='borrador', copy=False, required=True,
        track_visibility="onchange")
    clase_id = fields.Many2one(
        'clase', 'Clase', required=True, track_visibility="onchange")
    facilitador_id = fields.Many2one(
        'facilitador', 'Facilitador', required=True)
    facilitador_asistente = fields.Boolean(
        '¿Asistió Facilitador?', default=True, track_visibility="onchange")
    motivo_inasistencia = fields.Selection([
        ('personal', 'Diligencia Personal'),
        ('reposo', 'Reposo'),
        ('otros', 'Otros'),
    ], copy=False, track_visibility="onchange")
    clase_impartida = fields.Boolean(
        '¿Clase impartida?', default=True, track_visibility="onchange")
    horas_impartidas = fields.Float(
        'Horas Impartidas', help='Cantidad de horas impartidas en esta sesión',
        default=0)
    unidad_id = fields.Many2one(
        'unidad.curricular', 'Unidad Curricular',
        related='clase_id.unidad_id', store=True, readonly=True)
    formacion_id = fields.Many2one(
        'formacion', 'Formación',
        related='clase_id.formacion_id', store=True, readonly=True)
    fecha_inicio = fields.Date(
        'Fecha Inicio', related='clase_id.periodo_id.fecha_inicio',
        store=True, readonly=True)
    fecha_fin = fields.Date(
        'Fecha Fin', related='clase_id.periodo_id.fecha_fin',
        store=True, readonly=True)
    fecha_clase = fields.Date(
        'Fecha de la clase', required=True, default=fields.Date.today(),
        track_visibility="onchange", copy=False)
    linea_ids = fields.One2many(
        'asistencia.linea', 'asistencia_id', 'Linea de asistencias')
    observaciones = fields.Text(copy=False)
    total_asistentes = fields.Integer(
        'Total Asistentes', compute='_compute_asistentes_inasistentes',
        track_visibility="onchange")
    total_inasistentes = fields.Integer(
        'Total Inasistentes', compute='_compute_asistentes_inasistentes',
        track_visibility="onchange")

    _sql_constraints = [
        ('unique_asistencia',
         'unique(clase_id,fecha_clase)',
         'Solo debe haber una hoja de asistencias por fecha'),
    ]

    @api.onchange('clase_id')
    def _onchange_clase(self):
        return {
            'value': {
                'facilitador_id': self.clase_id.facilitador_id
            }
        }

    @api.onchange('facilitador_asistente')
    def _onchange_facilitador_asistente(self):
        return {
            'value': {
                'clase_impartida': self.facilitador_asistente,
                'horas_impartidas': 0,
                'motivo_inasistencia': False
            }
        }

    @api.onchange('clase_impartida')
    def _onchange_clase_impartida(self):
        if not self.clase_impartida:
            return {
                'value': {
                    'horas_impartidas': 0,
                }
            }

    @api.depends('clase_id', 'fecha_clase')
    def _compute_name(self):
        for record in self:
            fecha = datetime.strftime(record.fecha_clase, DATE_FORMAT)
            record.name = '{} - {}'.format(fecha, record.clase_id.name or '')

    @api.depends('linea_ids.asistente')
    def _compute_asistentes_inasistentes(self):
        linea_obj = self.env['asistencia.linea']
        for record in self:
            record.total_asistentes = linea_obj.search_count(
                [('asistente', '=', True), ('asistencia_id', '=', record.id)])
            record.total_inasistentes = linea_obj.search_count(
                [('asistente', '=', False), ('asistencia_id', '=', record.id)])

    @api.constrains('fecha_clase')
    def check_fecha_clase(self):
        today = fields.Date.today()
        for record in self:
            periodo = self.clase_id.periodo_id
            if record.fecha_clase < periodo.fecha_inicio:
                error = _(
                    "Fecha de la clase no está dentro del periodo definido")
                raise ValidationError(error)
            elif record.fecha_clase > today:
                error = _(
                    "No puede crear una hoja de asistencias con fecha futura")
                raise ValidationError(error)

    @api.constrains('facilitador_id')
    def check_fecilitador_id(self):
        for record in self:
            if record.facilitador_id != record.clase_id.facilitador_id:
                error = _(
                    "Facilitador no es el titular de la clase")
                raise ValidationError(error)

    def _check_excede_horas_unidad_curricular(self):
        horas_cargadas = sum(
            self.search([
                ('clase_id', '=', self.clase_id.id),
                ('clase_impartida', '=', True)]).mapped('horas_impartidas'))

        if horas_cargadas > self.unidad_id.duracion:
            error = _(
                "Carga horaria excedida para la clase según Unidad Curricular")
            raise ValidationError(error)
        return False

    @api.constrains('horas_impartidas')
    def check_horas_impartidas(self):
        for record in self:
            horas_cargadas = sum(self.search([
                ('clase_id', '!=', record.clase_id.id),
                ('facilitador_id', '=', record.facilitador_id.id),
                ('fecha_clase', '=', record.fecha_clase),
                ('clase_impartida', '=', True)]).mapped('horas_impartidas'))
            tipo_facilitador = record.facilitador_id.tipo_id
            if not tipo_facilitador:
                error = _(
                    "'%s' no tiene un tipo de facilitador establecido. Por \
                    favor, contacte al administrador del sistema") % (
                        record.facilitador_id.nombre_completo,)
                raise ValidationError(error)
            horas_laborables = tipo_facilitador.horas_laborables
            horas_restantes = (horas_laborables - horas_cargadas)
            if record.horas_impartidas < 0:
                error = _(
                    "Horas impartidas no pueden ser negativas")
                raise ValidationError(error)
            elif record.horas_impartidas > horas_restantes:
                error = _(
                    "Facilitador excede carga horaria del día. Se permite un \
                    máximo de %s horas diarias para '%s'") % (
                        horas_laborables, tipo_facilitador.name)
                raise ValidationError(error)
            elif record.clase_impartida and not record.horas_impartidas:
                error = _(
                    "Ingrese la cantidad de horas impartidas")
                raise ValidationError(error)
            else:
                record._check_excede_horas_unidad_curricular()

    @api.multi
    def set_aprobada(self):
        self.state = 'aprobada'

    @api.multi
    def set_borrador(self):
        self.state = 'borrador'

    @api.multi
    def write(self, vals):
        for record in self:
            if not record.facilitador_asistente:
                vals['clase_impartida'] = False
            super(Asistencia, record).write(vals)
        return True


class Facilitador(models.Model):
    _name = 'facilitador'
    _inherit = 'facilitador'

    asistencia_ids = fields.One2many(
        'asistencia', 'facilitador_id', 'Hojas de Asistencia')
