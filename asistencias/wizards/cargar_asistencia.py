from odoo import models, fields, api


class CargarAsistenciatWizard(models.TransientModel):
    _name = "asistencia.cargar_alumnos"
    _description = "Cargar Asistencia de Alumnos Wizard"

    asistencia_id = fields.Many2one(
        'asistencia', 'Asistencia',
        default=lambda self: self.env['asistencia'].browse(
            self.env.context['active_id']).id or False, readonly=True)
    clase_id = fields.Many2one(
        'clase', 'Clase',
        default=lambda self: self.env['asistencia'].browse(
            self.env.context['active_id']).clase_id.id or False, readonly=True)
    formacion_id = fields.Many2one(
        'formacion', 'Formación',
        default=lambda self: self.env['asistencia'].browse(
            self.env.context['active_id']).formacion_id.id or False,
        readonly=True)
    unidad_id = fields.Many2one(
        'unidad.curricular', 'Unidad Curricular',
        default=lambda self: self.env['asistencia'].browse(
            self.env.context['active_id']).unidad_id.id or False,
        readonly=True)
    fecha_clase = fields.Date(
        default=lambda self: self.env['asistencia'].browse(
            self.env.context['active_id']).fecha_clase or False,
        readonly=True)
    alumno_ids = fields.Many2many('alumno', string='Agregar alumno(s)')

    @api.multi
    def confirmar_alumnos(self):
        for record in self:
            for asistencia_id in self.env.context.get('active_ids', []):
                asistencia = self.env['asistencia'].browse(asistencia_id)
                alumnos = asistencia.clase_id.alumno_ids

                for alumno in alumnos:
                    vals = {'alumno_id': alumno.id, 'asistente': True,
                            'asistencia_id': asistencia.id}
                    if alumno.id in record.alumno_ids.ids:
                        vals.update({'asistente': False})
                    linea = self.env['asistencia.linea'].search([
                        ('asistencia_id', '=', asistencia.id),
                        ('alumno_id', '=', alumno.id),
                    ])
                    if linea:
                        linea.write(vals)
                    else:
                        self.env['asistencia.linea'].create(vals)
